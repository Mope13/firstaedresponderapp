﻿using System.Reflection;

[assembly: Xamarin.Forms.Dependency(typeof(RegionSyddanmark.Dependencies.ApplicationNameiOS))]
namespace RegionSyddanmark.Dependencies
{
    class ApplicationNameiOS
    {
        public string ApplicationName
        {
            get
            {
                object[] companyAttributes = (object[])typeof(ApplicationNameiOS).GetTypeInfo().Assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute));

                return ((AssemblyTitleAttribute)companyAttributes[0]).Title;
            }
        }

    }
}
