﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstAED.Dependencies;
using FirstAED.DependencyDefinitions;
using Foundation;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(BackgroundService))]
namespace FirstAED.Dependencies
{
    public class BackgroundService : IBackgroundService
    {
        public void PushToForeground()
        {
            //Do nothing..
        }
    }
}