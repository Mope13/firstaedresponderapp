﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using Xamarin.Forms;

namespace FirstAED.Behaviors
{
    public class AlarmController
    {
        public delegate void AlarmReceivedHandler(Models.Alarm alarm);
        public event AlarmReceivedHandler AlarmReceivedEvent;

        public enum AlarmControllerState
        {
            Idle,
            Active,
            AlarmActive
        }

        public AlarmControllerState currentAlarmControllerState = AlarmControllerState.Idle;

        private Timer t;
        private Services.AlarmService alarmService;

        public int currentAlarmInterval = Constants.Default.myAlarmsInterval;


        public AlarmController()
        {
            alarmService = new Services.AlarmService();
            t = new Timer(Constants.Default.myAlarmsInterval);
            t.Elapsed += T_Elapsed;
        }

        public void Start()
        {
            currentAlarmControllerState = AlarmControllerState.Active;
            t.Interval = currentAlarmInterval;
            t.Start();
        }

        public void Stop()
        {
            t.Stop();
            currentAlarmControllerState = AlarmControllerState.Idle;
        }

        private async void T_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                Models.Alarm myAlarm = await alarmService.GetActiveAlarm();

                if (myAlarm != null && AlarmReceivedEvent != null)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        AlarmReceivedEvent(myAlarm);
                    });
                    
                }

            }
            catch(Exception ex)
            {
                string test = "";
            }
        }
    }
}
