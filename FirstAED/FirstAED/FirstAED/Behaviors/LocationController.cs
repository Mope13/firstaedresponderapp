﻿using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FirstAED.Behaviors
{
    public class LocationController
    {
        private Services.LocationService locationService;

        public Plugin.Geolocator.Abstractions.Position CurrentPosition
        {
            get;
            private set;
        }

        public async void StartLocationListening()
        {
            if (!CrossGeolocator.Current.IsListening)
            {
                locationService = new Services.LocationService();

                CrossGeolocator.Current.DesiredAccuracy = 1;
                CrossGeolocator.Current.PositionChanged += Current_PositionChanged;
                await CrossGeolocator.Current.StartListeningAsync((Constants.Default.locationIntervalMinutes == Constants.Default.locationIntervalSecoundsActiveAlarm ? TimeSpan.FromSeconds(Constants.Default.locationIntervalMinutes) : TimeSpan.FromMinutes(Constants.Default.locationIntervalMinutes)), Constants.Default.locationIntervalDistanceDefault, true);
                await PushLocation();
            }
        }

        public async void UpdateLocationInterval(int time)
        {
            await CrossGeolocator.Current.StopListeningAsync();
            Constants.Default.locationIntervalMinutes = time;
            StartLocationListening();
        }

        private void Current_PositionChanged(object sender, Plugin.Geolocator.Abstractions.PositionEventArgs e)
        {
            if (CurrentPosition == null ||  (CrossGeolocator.Current.IsGeolocationAvailable && e.Position.Latitude != CurrentPosition.Latitude && e.Position.Longitude != CurrentPosition.Longitude))
            {
                CurrentPosition = e.Position;
                locationService.PushLocation(e.Position.Latitude, e.Position.Longitude, e.Position.Accuracy);
            }
        }

        public async Task<bool> PushLocation()
        {
            Plugin.Geolocator.Abstractions.Position pos = await CrossGeolocator.Current.GetPositionAsync();
            CurrentPosition = pos;
            Services.LocationService l = new Services.LocationService();
            l.PushLocation(pos.Latitude, pos.Longitude, pos.Accuracy);
            return true;
        }
    }
}