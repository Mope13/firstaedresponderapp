﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace FirstAED.Models
{

    [XmlRoot("alarm")]
    public class Alarm
    {
        public static void Initialize()
        {
            Kinds = new Dictionary<int, string>();

           switch(Constants.Default.region)
            {
                case "RSJ":
                    Kinds.Add(1, "");
                    Kinds.Add(2, "");
                    Kinds.Add(3, "Tilskadekomst"); //LOCALIZE
                    Kinds.Add(4, "");
                    Kinds.Add(5, "");
                    Kinds.Add(6, "Trafik/færdsel");
                    Kinds.Add(7, "Sygdom uspecificeret");
                    Kinds.Add(8, "Luftvej/Lungesygdom");
                    Kinds.Add(9, "Allergi");
                    Kinds.Add(10, "Hjertesygdom");
                    Kinds.Add(11, "Hjerneblødning");
                    Kinds.Add(12, "Kramper");
                    Kinds.Add(13, "Sukkersyge");
                    break;
                default:
                    Kinds.Add(1, "Hjertestop");
                    Kinds.Add(2, "Sygdom");
                    Kinds.Add(3, "Tilskadekomst");
                    Kinds.Add(4, "Trafik / Brand");
                    Kinds.Add(5, "Andet");
                    break;
            }
        }

        public static Dictionary<int, string> Kinds;

        //public enum Kinds
        //{
        //    HeartFailure = 1,
        //    Disease = 2,
        //    Injury = 3,
        //    TrafficFire = 4,
        //    Other = 5
        //}

        public enum Statuses
        {
            AlarmStarted = 0,
            MissingVolunteers = -1,
            AllVolunteersFound = 1,
            AlarmCompleted = 2
        }

        public string GetAlarmKind()
        {
            if (Kind == 0)
            {
                return "";
            }
            else
            {
                string kindText = Kinds[Kind];
                return kindText;
            }

            //switch(Kind)
            //{
            //    case (int)Kinds.HeartFailure:
            //        return "Hjertestop";
            //    case (int)Kinds.Disease:
            //        return "Sygdom";
            //    case (int)Kinds.Injury:
            //        return "Tilskadekomst";
            //    case (int)Kinds.TrafficFire:
            //        return "Trafik/brænd";
            //    case (int)Kinds.Other:
            //        return "Andet";
            //    default:
            //        return "";                       
            //}
        }

        public string GetAlarmInstructions()
        {
            int volunteerId = Constants.Default.volunteerId;
            Volunteer myVolunteer = Volunteers.SingleOrDefault(x => x.Id == volunteerId);
            if (myVolunteer != null)
                return myVolunteer.GetInstructions(Starter);
            else
                return "";
        }

        public Volunteer GetVolunteer()
        {
            return Volunteers.SingleOrDefault(x => x.Id == Constants.Default.volunteerId);
        }


        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlAttribute("kind")]
        public int Kind { get; set; }

        [XmlAttribute("created")]
        public string Created { get; set; }

        [XmlAttribute("notes")]
        public string Notes { get; set; }

        [XmlAttribute("status")]
        public int Status { get; set; }

        [XmlAttribute("address")]
        public string Address { get; set; }

        [XmlAttribute("lon")]
        public double Lon { get; set; }

        [XmlAttribute("lat")]
        public double Lat { get; set; }

        [XmlAttribute("secs")]
        public int Secs { get; set; }

        [XmlElement("volunteer")]
        public Volunteer[] Volunteers { get; set; }

        [XmlElement("starter")]
        public Starter Starter { get; set; }

        [XmlElement("contact")]
        public Contact[] Contacts { get; set; }
    }
}
