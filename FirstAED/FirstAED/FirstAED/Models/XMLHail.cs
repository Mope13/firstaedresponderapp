﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace FirstAED.Models
{
    [XmlRoot("hail")]
    public class XMLHail
    {
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlAttribute("host")]
        public string Host { get; set; }

        [XmlAttribute("port")]
        public int Port { get; set; }

        [XmlAttribute("code")]
        public double Code { get; set; }

        [XmlAttribute("active")]
        public double Active { get; set; }

        [XmlAttribute("medicine")]
        public int Medicine { get; set; }

        [XmlAttribute("oxygen")]
        public int Oxygen { get; set; }

        [XmlAttribute("receipt")]
        public int Receipt { get; set; }

        [XmlAttribute("sound")]
        public string Sound { get; set; }

    }
}
