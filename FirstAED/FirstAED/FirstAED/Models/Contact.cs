﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace FirstAED.Models
{
    [XmlRoot("contact")]
    public class Contact
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("phone")]
        public string Phone { get; set; }

        [XmlAttribute("secret")]
        public int Secret { get; set; }
    }
}
