﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace FirstAED.Models
{
    [XmlRoot("starter")]
    public class Starter
    {
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlAttribute("lat")]
        public double Lat { get; set; }

        [XmlAttribute("lon")]
        public double Lon { get; set; }

        [XmlAttribute("address")]
        public string Address { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("owner")]
        public string Owner { get; set; }

        [XmlAttribute("enabled")]
        public int Enabled { get; set; }

    }
}
