﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Xml.Serialization;

namespace FirstAED.Models
{
    [XmlRoot("alarms")]
    public class Alarms
    {
        [XmlAttribute("volunteer")]
        public int Volunteer { get; set; }
        
        [XmlElement("alarm")]
        public Alarm[] AlarmList { get; set; }
      //  [XmlArray("Alarm")]
     //   public ObservableCollection<Alarm> Alarm { get; set; }
    }
}
