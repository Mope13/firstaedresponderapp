﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace FirstAED.Models
{


    [XmlRoot("volunteer")]
    public class Volunteer
    {
        public enum Responses
        {
            VolunteerNotAnswered = 1,   //Volunteer/first-responser has been asked to participate and has not answered.
            VolunteerUnassign = 2,      //User has agreed to participate but system does not yet know what he should do.
            VolunteerEmergency = 3,     //User has agreed and has been assigned as first on scene and should get directly to emergency position.
            VolunteerAED = 4,           //User has agreed and has been assigned to get the closest AED (heart defillibrator) and then get to the emergency.
            VolunteerLast = 5,          //User has agreed as the last volunteer and should also proceed to the emergency position.
            VolunteerDecline = -2,      //User has answered that he/she cannot participate in alarm.
            VolunteerNotNeeded = -3,    //User has been asked to participate but participation is no longer needed, because other people closer to the emergency has been picked instead. This code can appear before the user answers or after answering yes.
            VolunteerObserving = -4     //User is observing alarm but no taking part. The address of the alarm should be shown.
        }

        public string GetInstructions(Starter starter) //Localization TODO
        {
            switch(Response)
            {
                case (int)Responses.VolunteerEmergency:
                case (int)Responses.VolunteerLast:
                    return "Tag til nødstedet";
                case (int)Responses.VolunteerAED:
                    if (starter == null)
                    {
                        return "Tag til nødstedet";
                    }
                    else
                    {
                        return "Hent hjertestarter og tag til nødstedet: " + starter.Address;
                    }
                    
                default:
                    return "";
            }
        }

        [XmlAttribute("id")]
        public int Id { get; set; }
        
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("lat")]
        public double Lat { get; set; }

        [XmlAttribute("lon")]
        public double Lon { get; set; }

        [XmlAttribute("address")]
        public string Address { get; set; }

        [XmlAttribute("phone")]
        public string Phone { get; set; }

        [XmlAttribute("active")]
        public double Active { get; set; }
        
        [XmlAttribute("p")]
        public double P { get; set; }

        [XmlAttribute("response")]
        public int Response { get; set; }

        [XmlAttribute("duration")]
        public double Duration { get; set; }

        [XmlAttribute("s_lat")]
        public double S_lat { get; set; }

        [XmlAttribute("s_lon")]
        public double S_lon { get; set; }
    }
}
