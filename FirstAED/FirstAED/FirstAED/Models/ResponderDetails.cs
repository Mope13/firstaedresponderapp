﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstAED.Models
{
    public class ResponderDetails
    {
        public string Responder { get; private set; }
        public bool Secret { get; private set; }
        public string PhoneNumber { get; private set; }

        public string SecretPhoneNumber { get; set; }

        public ResponderDetails(string responder, bool secret, string phoneNumber)
        {
            this.Responder = responder;
            this.Secret = secret;
            this.PhoneNumber = (secret ? "" : phoneNumber);
            this.SecretPhoneNumber = phoneNumber;
        }
    }
}
