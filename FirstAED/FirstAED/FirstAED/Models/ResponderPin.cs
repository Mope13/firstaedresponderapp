﻿using FirstAED.CustomControllers;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace FirstAED.Models
{
    public class ResponderPin
    {
        public Pin Pin { get; set; }
        public string Id { get; set; }
        public CustomPinType CustomPinType { get; set; }
    }
}
