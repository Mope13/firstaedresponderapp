﻿using FirstAED.CustomControllers;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstAED.Models
{
    public class HeartStarter
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Address { get; set; }
    }
}
