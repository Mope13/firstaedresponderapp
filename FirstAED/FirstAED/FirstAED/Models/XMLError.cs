﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace FirstAED.Models
{
    [XmlRoot("error")]
    public class XMLError
    {
        [XmlAttribute("code")]
        public int Code { get; set; }

        [XmlAttribute("message")]
        public string Message { get; set; }
    }
}
