﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace FirstAED.Models
{
    [XmlRoot("starters")]
    public class Starters
    {
        [XmlElement("starter")]
        public Starter[] StarterList { get; set; }
    }
}
