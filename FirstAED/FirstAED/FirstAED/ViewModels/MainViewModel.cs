﻿using FirstAED.Views;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace FirstAED.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        INavigation Navigation;
        CaseReportPage caseReportPage;

        public ICommand CaseReportCommand { private set; get; }

        public event PropertyChangedEventHandler PropertyChanged;
        public MainViewModel(INavigation navigation)
        {
            Navigation = navigation;

            CaseReportCommand = new Command(OnCaseReportClick);
        }

        async void OnCaseReportClick()
        {
            caseReportPage = new CaseReportPage();
            NavigationPage.SetHasNavigationBar(caseReportPage, false);
            await Navigation.PushAsync(caseReportPage);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
