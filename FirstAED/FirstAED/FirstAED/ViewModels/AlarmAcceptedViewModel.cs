﻿using FirstAED.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace FirstAED.ViewModels
{
    public class AlarmAcceptedViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        //public ObservableCollection<ResponderDetails> ContactsSource { get; set; }


        public AlarmAcceptedViewModel()
        {
            //this.ContactsSource = new ObservableCollection<ResponderDetails> { new ResponderDetails("Responder 1", "First on site", "22762124"), new ResponderDetails("Responder 2", "Get AED", "11335577") };
        }


        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
