﻿using FirstAED.DependencyDefinitions;
using FirstAED.Models;
using FirstAED.Services;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace FirstAED.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<string> AlertMessages { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public INavigation Navigation { get; set; }
        public string PasswordEntry { get; set; }
        ILoginService loginService;
        IHailService hailService;
        public ICommand LoginCommand { private set; get; }
        public string TestText { get; set; }

        private static ISettings AppSettings => CrossSettings.Current;

        static bool IsUserSet => AppSettings.Contains(nameof(AuthCode));

        static string AuthCode
        {
            get => AppSettings.GetValueOrDefault(nameof(AuthCode), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(AuthCode), value);
        }

        public LoginViewModel(INavigation navigation)
        {
            //#if __ANDROID__
            //PasswordEntry = "20472438";
            //#endif
            //#if __IOS__
            //PasswordEntry = "02429113";
            //#endif

            if(IsUserSet)
            {
                PasswordEntry = AuthCode;
             
            }
            
            AlertMessages = new ObservableCollection<string>();
            loginService = new LoginService();
            hailService = new HailService();
            Navigation = navigation;

            LoginCommand = new Command(LoginClick);
            TestText = L10n.Localize("login", "login");
        }

        public async void LoginClick()
        {
            try
            {
                XMLHail response;

                response = await hailService.HailServer(PasswordEntry);

                Constants.Default.locationController = new Behaviors.LocationController();
                Constants.Default.locationController.StartLocationListening();
                await Constants.Default.locationController.PushLocation();

                Constants.Default.sc = PasswordEntry;
                AuthCode = PasswordEntry;
                Constants.Default.volunteerId = response.Id;
                DateTime whenActive = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                whenActive = whenActive.AddSeconds(response.Active).ToLocalTime();

                Constants.Default.whenActive = (DateTime.Now > whenActive ? "Aktiv nu" : "Aktiv fra: " + whenActive.ToString());

                Constants.Default.alarmController = new Behaviors.AlarmController();
                MainPage mainPage = new MainPage();

                await Navigation.PushAsync(mainPage);
            }
            catch (Exception ex)
            {
                MessagingCenter.Send<LoginViewModel, string>(this, "Fejl", ex.Message); //LOCALIZE
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
