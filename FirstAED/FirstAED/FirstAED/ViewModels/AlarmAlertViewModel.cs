﻿using FirstAED.DependencyDefinitions;
using FirstAED.Models;
using FirstAED.Views;
using Geolocation;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Plugin.Vibrate;
using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Timers;
using System.Windows.Input;
using Xamarin.Forms;

namespace FirstAED.ViewModels
{ //TODO Localization
    public class AlarmAlertViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private static ISettings AppSettings => CrossSettings.Current;

        public INavigation _Navigation { get; set; }
        string timeLabel;
        string questionLabel;
        string distanceLabel;

        public string TimeLabel
        {
            set
            {
                timeLabel = value;
                OnPropertyChanged("TimeLabel");
            }
            get
            {
                return timeLabel;
            }
        }

        public string QuestionLabel
        {
            set
            {
                questionLabel = value;
                OnPropertyChanged("QuestionLabel");
            }
            get
            {
                return questionLabel;
            }
        }

        public string DistanceLabel
        {
            set
            {
                distanceLabel = value;
                OnPropertyChanged("DistanceLabel");
            }
            get
            {
                return distanceLabel;
            }
        }

        Alarm myAlarm;
        Timer t;

        public ICommand NoSlideCompletedCommand { get; private set; }
        public ICommand YesSlideCompletedCommand { get; private set; }

        static string SoundIsAlarm
        {
            get => AppSettings.GetValueOrDefault(nameof(SoundIsAlarm), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(SoundIsAlarm), value);
        }

        public AlarmAlertViewModel(INavigation Navigation, Alarm alarm)
        {
            _Navigation = Navigation;
            myAlarm = alarm;
            InitializeView();
            NoSlideCompletedCommand = new Command(NoSlideCompleted);
            YesSlideCompletedCommand = new Command(YesSlideCompleted);

            CrossVibrate.Current.Vibration(TimeSpan.FromSeconds(2));
            t = new Timer(2500);
            t.Elapsed += T_Elapsed;
            t.Start();


            switch (SoundIsAlarm)
            {
                case "alarm":
                    DependencyService.Get<IAudioService>().PlayAlarm("alarm");
                    break;
                case "siren":
                    DependencyService.Get<IAudioService>().PlayAlarm("siren");
                    break;
            }
        }

        private void T_Elapsed(object sender, ElapsedEventArgs e)
        {
            CrossVibrate.Current.Vibration(TimeSpan.FromSeconds(2));
        }

        private void InitializeView()
        {
            string time = myAlarm.Created.Split(' ')[1];
            var test = Constants.Default.locationController.CurrentPosition;
            Behaviors.DistanceCalculator dc = new Behaviors.DistanceCalculator();



            double distance = dc.Distance(Constants.Default.locationController.CurrentPosition.Latitude, Constants.Default.locationController.CurrentPosition.Longitude, myAlarm.Lat, myAlarm.Lon, 'K');

            string direction = GeoCalculator.GetDirection(Constants.Default.locationController.CurrentPosition.Latitude, Constants.Default.locationController.CurrentPosition.Longitude, myAlarm.Lat, myAlarm.Lon);

            switch (direction)
            {
                case "N":
                    direction = " nord"; //LOCALIZE
                    break;
                case "NE":
                    direction = " nord øst";
                    break;
                case "NW":
                    direction = " nord vest";
                    break;
                case "S":
                    direction = " syd";
                    break;
                case "SE":
                    direction = " syd øst";
                    break;
                case "SW":
                    direction = " syd vest";
                    break;
                case "E":
                    direction = " øst";
                    break;
                case "W":
                    direction = " vest";
                    break;
            }

            // Device.BeginInvokeOnMainThread(() =>
            // {
            TimeLabel = time;
            DistanceLabel = "Ca. " + Math.Round(distance, 2).ToString() + " km" + direction;
            QuestionLabel = "Vil du deltage i alarm?";
            //  });



        }

        async void YesSlideCompleted()
        {
            t.Stop();
            t.Elapsed -= T_Elapsed;
            t.Dispose();

            //DependencyService.Get<IAudioService>().StopAlarm();

            TimeLabel = "";
            DistanceLabel = "";
            QuestionLabel = "Please wait...";


            Services.AlarmService alarmService = new Services.AlarmService();
            await alarmService.RespondAlarm(myAlarm, true);

            Constants.Default.alarmController.AlarmReceivedEvent += AlarmController_AlarmReceivedEvent;


        }

        private void AlarmController_AlarmReceivedEvent(Alarm alarm)
        {
            int responseCode = alarm.Volunteers.SingleOrDefault(x => x.Id == Constants.Default.volunteerId).Response;

            switch (responseCode)
            {
                case (int)Volunteer.Responses.VolunteerUnassign:
                case (int)Volunteer.Responses.VolunteerEmergency:
                case (int)Volunteer.Responses.VolunteerAED:
                case (int)Volunteer.Responses.VolunteerLast:
                    Constants.Default.alarmController.AlarmReceivedEvent -= AlarmController_AlarmReceivedEvent;

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        AlarmAcceptedPage alarmAcceptedPage = new AlarmAcceptedPage();
                        NavigationPage.SetHasNavigationBar(alarmAcceptedPage, false);

                        _Navigation.PushAsync(alarmAcceptedPage);
                    });
                    break;
                default:
                    t.Stop();
                    t.Dispose();
                    Constants.Default.alarmController.AlarmReceivedEvent -= AlarmController_AlarmReceivedEvent;
                    Constants.Default.alarmController.Stop();
                    _Navigation.PopAsync();
                    break;
            }


        }

        async void NoSlideCompleted()
        {
            t.Stop();
            t.Elapsed -= T_Elapsed;
            t.Dispose();

            Constants.Default.alarmController.Stop();

            Services.AlarmService alarmService = new Services.AlarmService();
            await alarmService.RespondAlarm(myAlarm, false);

            await _Navigation.PopAsync();
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
