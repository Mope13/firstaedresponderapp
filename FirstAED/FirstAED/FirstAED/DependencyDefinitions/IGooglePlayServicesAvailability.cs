﻿namespace FirstAED.DependencyDefinitions
{
    public interface IGooglePlayServicesAvailability
    {
        string IsPlayServicesAvailable();
    }
}
