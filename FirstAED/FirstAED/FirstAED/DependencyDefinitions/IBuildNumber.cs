﻿namespace FirstAED.DependencyDefinitions
{
    public interface IBuildNumber
    {
        string Version { get; }
    }
}
