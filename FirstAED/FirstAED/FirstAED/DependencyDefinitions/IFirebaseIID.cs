﻿namespace FirstAED.DependencyDefinitions
{
    public interface IFirebaseIID
    {
        string GetFirebaseIID();
    }
}
