﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstAED.DependencyDefinitions
{
    public interface IBackgroundService
    {
        void PushToForeground();
    }
}
