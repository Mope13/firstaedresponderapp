﻿namespace FirstAED.DependencyDefinitions
{
    public interface IAudioService
    {
        void PlayAlarm(string sound);

        void StopAlarm();
    }
}
