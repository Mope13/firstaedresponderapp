﻿using Xamarin.Forms.Maps;

namespace FirstAED.CustomControllers
{
    class CustomizedPin : Pin
    {
        public string CustomPinType { get; set; }
        public string Url { get; set; }
    }
}
