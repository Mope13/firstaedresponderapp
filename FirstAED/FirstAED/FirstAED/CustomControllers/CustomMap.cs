﻿using FirstAED.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms.Maps;

namespace FirstAED.CustomControllers
{
    public class CustomMap : Map
    {
        public ObservableCollection<HeartStarterPin> HeartStarterPins { get; set; }

        public ObservableCollection<AlarmPin> AlarmPins { get; set; }

        public ObservableCollection<ResponderPin> ResponderPins { get; set; }

        public bool UpdateMapPins = true;

        public ObservableCollection<Position> RouteCoordinates { get; set; }

        public CustomMap()
        {
            RouteCoordinates = new ObservableCollection<Position>();
        }
    }
}
