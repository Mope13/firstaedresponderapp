﻿using System.Collections.ObjectModel;
using Xamarin.Forms.Maps;

namespace FirstAED.CustomControllers
{
    class EmergencyMap : Map
    {
        public ObservableCollection<CustomizedPin> CustomizedPins { get; set; }
    }
}
