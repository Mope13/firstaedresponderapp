﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FirstAED.CustomControllers
{
    public class ToggleImage : Grid
    {
        public static readonly BindableProperty ToggledSourceProperty = BindableProperty.Create("ToggledSource", typeof(ImageSource), typeof(Image), default(ImageSource), propertyChanged: OnValueChanged);
        public static readonly BindableProperty OriginalSourceProperty = BindableProperty.Create("OriginalSource", typeof(ImageSource), typeof(Image), default(ImageSource), propertyChanged: OnValueChanged);
        public static readonly BindableProperty IsToggledProperty = BindableProperty.Create("IsToggled", typeof(bool), typeof(Image), false, defaultBindingMode: BindingMode.TwoWay, propertyChanged: OnValueChanged);

        private Image _originalImage;
        private Image _toggledImage;

        public ToggleImage()
        {
            var tapRecognizer = new TapGestureRecognizer();
            tapRecognizer.Command = new Command(() => IsToggled = !IsToggled);
            GestureRecognizers.Add(tapRecognizer);

            _originalImage = new Image { Aspect = Aspect.AspectFit };
            _toggledImage = new Image { Aspect = Aspect.AspectFit };

            Children.Add(_originalImage);
            Children.Add(_toggledImage);
        }

        static void OnValueChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var ctrl = bindable as ToggleImage;
            if (ctrl == null)
                return;

            ctrl.OnValueChanged();
        }

        void OnValueChanged()
        {
            _originalImage.Source = OrginalSource;
            _toggledImage.Source = ToggledSource;

            _toggledImage.IsVisible = IsToggled;
            _originalImage.IsVisible = !IsToggled;
        }

        public bool IsToggled
        {
            get { return (bool)GetValue(IsToggledProperty); }
            set { SetValue(IsToggledProperty, value); }
        }

        public ImageSource ToggledSource
        {
            get { return (ImageSource)GetValue(ToggledSourceProperty); }
            set { SetValue(ToggledSourceProperty, value); }
        }

        public ImageSource OrginalSource
        {
            get { return (ImageSource)GetValue(OriginalSourceProperty); }
            set { SetValue(OriginalSourceProperty, value); }
        }
    }
}
