﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FirstAED.CustomControllers
{
    public class ExpanderMenu : AbsoluteLayout
    {
        public static readonly BindableProperty MenuProperty =
            BindableProperty.Create(
                "Menu", typeof(View), typeof(ExpanderMenu),
                defaultValue: default(View), propertyChanged: OnMenuChanged);

        public View Menu
        {
            get { return (View)GetValue(MenuProperty); }
            set { SetValue(MenuProperty, value); }
        }

        private static void OnMenuChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((ExpanderMenu)bindable).OnMenuChangedImpl((View)oldValue, (View)newValue);
        }

        protected virtual void OnMenuChangedImpl(View oldValue, View newValue)
        {
            OnSizeChanged(this, EventArgs.Empty);
        }

        public static readonly BindableProperty RoundButtonProperty =
            BindableProperty.Create(
                "RoundButton", typeof(View), typeof(ExpanderMenu),
                defaultValue: default(View), propertyChanged: OnRoundButtonChanged);

        public View RoundButton
        {
            get { return (View)GetValue(RoundButtonProperty); }
            set { SetValue(RoundButtonProperty, value); }
        }

        private static void OnRoundButtonChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((ExpanderMenu)bindable).OnRoundButtonChangedImpl((View)oldValue, (View)newValue);
        }

        protected virtual void OnRoundButtonChangedImpl(View oldValue, View newValue)
        {
            OnSizeChanged(this, EventArgs.Empty);
        }

        public ExpanderMenu()
        {
            SizeChanged += OnSizeChanged;
        }

        void OnSizeChanged(object sender, EventArgs e)
        {
            if (Width == 0 || Height == 0)
                return;
            if (Menu == null || RoundButton == null)
                return;


            Children.Clear();

            SetLayoutFlags(Menu, AbsoluteLayoutFlags.SizeProportional);
            SetLayoutBounds(Menu, new Rectangle(0, 0, 1, 1));
            Children.Add(Menu);

            SetLayoutFlags(RoundButton, AbsoluteLayoutFlags.SizeProportional);
            SetLayoutBounds(RoundButton, new Rectangle(0, 0, 1, 1));
            Children.Add(RoundButton);
        }
    }
}
