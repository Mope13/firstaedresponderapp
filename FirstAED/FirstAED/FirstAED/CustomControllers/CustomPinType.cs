﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstAED.CustomControllers
{
    public enum CustomPinType
    {
        Heartstarter = 0,
        Alarm = 1,
        Responder = 2
    }
}
