﻿using FirstAED.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using static FirstAED.Models.StarterDirections;

namespace FirstAED.Services
{
    class GoogleDirectionsService : IGoogleDirectionsService
    {
        public async Task<StarterDirections> GetDirections(double originLat, double originLong, double destinationLat, double destinationLong)
        {
            StarterDirections starterDirections = new StarterDirections();

            using (HttpClient client = new HttpClient())
            {
                var uri = new Uri(string.Format("https://maps.googleapis.com/maps/api/directions/json?origin={0}+{1}&destination={2}+{3}&mode=walking&key={4}", originLat.ToString().Replace(',', '.'), originLong.ToString().Replace(',', '.'), destinationLat.ToString().Replace(',', '.'), destinationLong.ToString().Replace(',', '.'), Constants.Default.apiKey));

                var response = await client.GetAsync(uri);


                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    starterDirections = JsonConvert.DeserializeObject<StarterDirections>(content);
                }
                return starterDirections;
            }
        }
    }
}
