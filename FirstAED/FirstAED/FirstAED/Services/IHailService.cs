﻿using FirstAED.Models;
using System.Threading.Tasks;

namespace FirstAED.Services
{
    public interface IHailService
    {
        Task<XMLHail> HailServer(string sc);
    }
}
