﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace FirstAED.Services
{
    public class SetActiveService : ISetActiveService
    {
        public async Task<bool> SetActive(DateTime when)
        {
            DateTimeOffset offset = new DateTimeOffset(when);
            Uri uri = new Uri(Constants.Default.hostUrl + string.Format("set-active.php?sc={0}&when={1}", Constants.Default.sc, offset.ToUnixTimeSeconds().ToString()));

            using (HttpClient client = new HttpClient())
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
