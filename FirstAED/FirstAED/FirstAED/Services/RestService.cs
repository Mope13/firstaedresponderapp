﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;
using FirstAED.Models;
using System.Collections.ObjectModel;

namespace FirstAED.Services
{
    public class RestService : IRestService
    {

        //public ObservableCollection<HeartStarter> HeartStarters { get; set; }

        public async Task<Starter[]> GetHeartStarters()
        {

            using (HttpClient client = new HttpClient())
            {
                Starters myStarters = null;

                var uri = new Uri(Constants.Default.hostUrl + "starters.php?sc=" + Constants.Default.sc);

                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    myStarters = XmlDeSerializer.DeSerializeObject<Models.Starters>(content);
                    //HeartStarters = JsonConvert.DeserializeObject<ObservableCollection<HeartStarter>>(content);
                }
                return myStarters.StarterList;
            }
        }
    }
}
