﻿using FirstAED.DependencyDefinitions;
using FirstAED.Models;
using Plugin.DeviceInfo;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FirstAED.Services
{
    class HailService : IHailService
    {
        XMLHail xmlHail;
        string deviceID;
        string platform;
        string version;
        string pushID;


        private static ISettings AppSettings => CrossSettings.Current;

        public static string AuthCode
        {
            get => AppSettings.GetValueOrDefault(nameof(AuthCode), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(AuthCode), value);
        }

        public async Task<XMLHail> HailServer(string sc)
        {
            deviceID = CrossDeviceInfo.Current.Id.ToString();

            if (CrossDeviceInfo.Current.Platform.ToString() == "Android")
            {
                platform = "a";
            }
            else if (CrossDeviceInfo.Current.Platform.ToString() == "iOS")
            {
                platform = "i";
            }

            version = DependencyService.Get<IBuildNumber>().Version;


            pushID = Plugin.FirebasePushNotification.CrossFirebasePushNotification.Current.Token; // DependencyService.Get<IFirebaseIID>().GetFirebaseIID();


            var uri = new Uri(Constants.Default.hostUrl + "hail.php?" + "ver=" + platform + "-" + version + "&push=" + pushID + "&udid=" + deviceID + "&sc=" + sc);


            using (HttpClient client = new HttpClient())
            {

                Debug.WriteLine("ATTENTION" + uri.ToString());

                var response = await client.GetAsync(uri);
                //if (response.IsSuccessStatusCode == false) //TODO
                //{
                var content = await response.Content.ReadAsStringAsync();

                if (content.Contains("<error"))
                {
                    XMLError error = XmlDeSerializer.DeSerializeObject<XMLError>(content);
                    throw new Exception(error.Message);
                }
                else
                {
                    xmlHail = XmlDeSerializer.DeSerializeObject<XMLHail>(content);
                    AuthCode = sc;
                }
                //}
                return xmlHail;
            }
        }
    }
}
