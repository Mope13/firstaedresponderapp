﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FirstAED.Services
{
    public interface ILoginService
    {
        Task GetLogin(string authCode);
    }
}
