﻿using FirstAED.Models;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FirstAED.Services
{
    class AlarmService : IAlarmService
    {


        public async Task<Alarms> GetAlarms()
        {
            var uri = new Uri(Constants.Default.hostUrl + "my-alarms.php?sc=" + Constants.Default.sc);
            Alarms alarms = null;
            
            using (HttpClient client = new HttpClient())
            {


                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                     alarms = XmlDeSerializer.DeSerializeObject<Alarms>(content);
                }
                return alarms;
            }
        }

        public async Task<Alarm> GetActiveAlarm()
        {
            Alarms myAlarms = await GetAlarms();

            // Alarm myAlarm = myAlarms.AlarmList.SingleOrDefault(x => x.Status != 2 && x.Status != 1 && (x.Volunteers.SingleOrDefault(y => y.Id == myAlarms.Volunteer) != null));
            Alarm myAlarm = null;

            if (myAlarms.AlarmList != null)
            {
                myAlarm = myAlarms.AlarmList.First(); //TEMP
            }

            if (myAlarm != null)
            {
                return myAlarm;
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> RespondAlarm(Alarm alarm, bool acceptAlarm)
        {
            int alarmId = alarm.Id;
            var uri = new Uri(Constants.Default.hostUrl +  string.Format("respond-alarm.php?sc={0}&alarm={1}&accept={2}",Constants.Default.sc ,alarmId, (acceptAlarm ? "1" : "0")));
            using (HttpClient client = new HttpClient())
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    return true;
                }
                else
                {
                    return false;
                }
            }

        }
    }
}

