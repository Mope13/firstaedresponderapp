﻿using FirstAED.Models;
using System.Threading.Tasks;
using static FirstAED.Models.StarterDirections;

namespace FirstAED.Services
{
    public interface IGoogleDirectionsService
    {
        Task<StarterDirections> GetDirections(double originLong, double originLat, double destinationLong, double destinationLat);
    }
}
