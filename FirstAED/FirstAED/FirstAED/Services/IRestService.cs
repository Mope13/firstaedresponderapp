﻿using FirstAED.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace FirstAED.Services
{
    public interface IRestService
    {
        Task<Starter[]> GetHeartStarters();
    }
}
