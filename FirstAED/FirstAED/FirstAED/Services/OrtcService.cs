﻿//using Realtime.Messaging;
//using System;
//using FirstAED.Models;
//using System.Diagnostics;
//using FirstAED.DependencyDefinitions;
//using Xamarin.Forms;
//using FirstAED.Views;
//using Newtonsoft.Json;

//namespace FirstAED.Services
//{

//    public class OrtcService
//    {
//        public delegate void DataReceivedHandler(MessageReponse mr);
//        public event DataReceivedHandler DataReceived;

//        MainPage mainPage;
//        AlarmAlertPage alarmAlertPage;
//        AlarmAlert alarmAlert;
//        public INavigation Navigation { get; set; }
//        OrtcClient ortcClient;
//        string applicationKey = "T96c7c";
//        string authenticationToken = "yourAuthenticationToken";

//        public OrtcService(MainPage MainPage, INavigation Navigation)
//        {
//            return;
//            this.Navigation = Navigation;
//            mainPage = MainPage;
//            ortcClient = new OrtcClient();
//            ortcClient.OnConnected += OrtcClient_OnConnected;
//            ortcClient.OnException += OrtcClient_OnException;
//            ortcClient.ClusterUrl = "http://ortc-developers.realtime.co/server/2.1/";

//            ortcClient.Connect(applicationKey, authenticationToken);
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="sender"></param>
//        /// <param name="ex"></param>
//        private void OrtcClient_OnException(object sender, Exception ex)
//        {
//            Debug.WriteLine(ex.Message);
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="sender"></param>
//        private void OrtcClient_OnConnected(object sender)
//        {
//            ortcClient.Subscribe("readyChannel", true, OnMessageCallback);
//        }

//        /// <summary>
//        /// Behandler besked modtaget fra MyChannel
//        /// </summary>
//        /// <param name="sender"></param>
//        /// <param name="channel"></param>
//        /// <param name="message"></param>
//        private void OnMessageCallback(object sender, string channel, string message)
//        {
//            //DependencyService.Get<ILocalNotification>().GetLocalNotification(message);
//            //alarmAlert = JsonConvert.DeserializeObject<AlarmAlert>(message);

//            //alarmAlertPage = new AlarmAlertPage(Navigation, alarmAlert.Id.ToString());
//            //NavigationPage.SetHasNavigationBar(alarmAlertPage, false);
//            //Navigation.PushAsync(alarmAlertPage);
//            //Debug.WriteLine("Message received: " + alarmAlert.Id.ToString());
//        }
//    }
//}
