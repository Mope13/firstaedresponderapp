﻿using System;
using System.Threading.Tasks;

namespace FirstAED.Services
{
    public interface ISetActiveService
    {
        Task<bool> SetActive(DateTime when);
    }
}
