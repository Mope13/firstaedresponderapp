﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace FirstAED.Services
{
    public static class XmlDeSerializer
    {
        public static T DeSerializeObject<T>(string xml)
        {
            if (string.IsNullOrEmpty(xml)) return default(T);

            T objectOut = default(T);

            try
            {
                using (StringReader read = new StringReader(xml))
                {
                    Type outType = typeof(T);

                    XmlSerializer serializer = new XmlSerializer(outType);
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        objectOut = (T)serializer.Deserialize(reader);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return objectOut;
        }

    }
}
