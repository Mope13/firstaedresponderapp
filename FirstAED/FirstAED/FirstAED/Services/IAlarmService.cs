﻿using FirstAED.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FirstAED.Services
{
    public interface IAlarmService
    {
        Task<Alarms> GetAlarms();

        Task<Alarm> GetActiveAlarm();
    }
}
