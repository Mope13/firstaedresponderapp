﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using PCLStorage;
using Newtonsoft.Json;
using FirstAED.Models;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace FirstAED.Services
{
    public class LoginService : ILoginService
    {

        User user;

        private static ISettings AppSettings => CrossSettings.Current;

        public static string UserName
        {
            get => AppSettings.GetValueOrDefault(nameof(UserName), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(UserName), value);
        }

        public static string URL
        {
            get => AppSettings.GetValueOrDefault(nameof(URL), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(URL), value);
        }

        public static string AuthCode
        {
            get => AppSettings.GetValueOrDefault(nameof(AuthCode), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(AuthCode), value);
        }

        public async Task GetLogin(string authCode)
        {
            user = new User();
            using (HttpClient client = new HttpClient())
            {
                var uri = new Uri("http://104.40.213.91/api/codeauthentication.php?code=" + authCode);

                var response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    user = JsonConvert.DeserializeObject<User>(content);

                }
                if (user.success)
                {
                    UserName = user.name;
                    URL = user.url;
                    AuthCode = authCode;
                    //await StorageTask(user.name, user.url, authCode);
                }
                else
                {
                    return;
                }
            }
        }

        public async Task StorageTask(string Name, string URL, string authcode)
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder folder = await rootFolder.CreateFolderAsync("FirstAEDFolder", CreationCollisionOption.OpenIfExists);
            IFile file = await folder.CreateFileAsync("FirstAEDUserDetails", CreationCollisionOption.ReplaceExisting);
            await file.WriteAllTextAsync(Name + URL + authcode);
        }
    }
}
