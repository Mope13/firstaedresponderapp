﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using Plugin.DeviceInfo;

namespace FirstAED.Services
{
    public class LocationService
    {
        public async void PushLocation(double latitude, double longitude, double precision)
        {
            string deviceID = CrossDeviceInfo.Current.Id.ToString();
            string time = DateTime.Now.Ticks.ToString();

            
            using (HttpClient client = new HttpClient())
            {
                string url = string.Format(Constants.Default.hostUrl + "location.php?lat={0}&lon={1}&p={2}&ts={3}&mode={4}&udid={5}&sc={6}&alarm={7}", latitude.ToString().Replace(',','.'),longitude.ToString().Replace(',', '.'), precision.ToString().Replace(',', '.'),time,Constants.Default.locationMode, deviceID, Constants.Default.sc, Constants.Default.currentAlarmId);
                HttpResponseMessage response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();  
                }   
            }
        }
    }
}
