﻿using FirstAED.DependencyDefinitions;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using Xamarin.Forms;

namespace FirstAED.Constants
{
    public static class Default
    {
        private static ISettings AppSettings => CrossSettings.Current;

        public static string UserName
        {
            get => AppSettings.GetValueOrDefault(nameof(UserName), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(UserName), value);
        }

        public static string URL
        {
            get => AppSettings.GetValueOrDefault(nameof(URL), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(URL), value);
        }

        public static string AuthCode
        {
            get => AppSettings.GetValueOrDefault(nameof(AuthCode), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(AuthCode), value);
        }

        public static string version = DependencyService.Get<IBuildNumber>().Version;

        //public static string hostUrl = "https://udkaldssystem.rsyd.dk/";
        //public static string hostUrl = "https://test.firstaed.org/";
        public static string hostUrl = "https://app-rsj.firstaed.org/";

        //public static string region = "NOTRSJ"; //TEMP
        public static string region = "RSJ"; //TEMP

        public static string sc = "";
        public static int volunteerId = 0;
        public static string whenActive = "";

        #region Alarm

        public static int currentAlarmId = 0;
        public static int myAlarmsInterval = 5000;
        public static Behaviors.AlarmController alarmController;

        #endregion

        #region Location

        public static int locationMode = 2;

        public static int locationIntervalMinutes = locationIntervalMinutesDefault;

        public const int locationIntervalMinutesDefault = 31;

        public const int locationIntervalDistanceDefault = 50;

        public const int locationIntervalSecoundsActiveAlarm = 30;

        public const string apiKey = "AIzaSyAJVItJ4a0xKxMSMH0kCIapWEGzqpK0rec";

        public static Behaviors.LocationController locationController;

        #endregion
    }
}
