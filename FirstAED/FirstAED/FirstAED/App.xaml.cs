﻿using FirstAED.Models;
using FirstAED.Services;
using FirstAED.Views;
using Xamarin.Forms;

namespace FirstAED
{
    public partial class App : Application
    {
        //OrtcService ortcService;
       // MainPage mainPage;
        LoginPage loginPage;
        INavigation navigation;
        IRegionServerCheckService RSCService;
        RegionServerAuthCode RSACode;

       // IFileSystem FirstAEDFileSystem { get { return FileSystem.Current; } }

       // public static HeartStarterLoad HeartStart { get; set; }
        //public static UserLoad UserTest { get; set; }


        public App()
        {
            InitializeComponent();


            RSCService = new RegionServerCheckService(RSACode);
            IHailService hailService = new HailService();

            Models.Alarm.Initialize();


            // HeartStart = new HeartStarterLoad(new RestService());

            // ortcService = new OrtcService(mainPage, navigation);

            //mainPage = new MainPage(ortcService);
            //loginPage = new LoginPage(mainPage);
            loginPage = new LoginPage();

            MainPage = new NavigationPage(loginPage);
            NavigationPage.SetHasNavigationBar(loginPage, false);

#if __ANDROID__
//Måske firebase her?
#endif
        }

        protected /*async*/ override void OnStart()
        {
            //mainPage = new MainPage(ortcService);
            //loginPage = new LoginPage(mainPage);
            
            ////RSACode = await RSCService.AuthenticateUser(AuthCode);
            //XMLHail response;

            //if (IsUserSet /*&& RSACode.Success*/)
            //{
            //    response = await hailService.HailServer(AuthCode);

            //    MainPage = new NavigationPage(mainPage);
            //    NavigationPage.SetHasNavigationBar(mainPage, false);
            //}
            //else
            //{
            //    MainPage = new NavigationPage(mainPage); //TEMP
            //    NavigationPage.SetHasNavigationBar(mainPage, false);

            //    //MainPage = new NavigationPage(loginPage);
            //    //NavigationPage.SetHasNavigationBar(loginPage, false);
            //}

        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
