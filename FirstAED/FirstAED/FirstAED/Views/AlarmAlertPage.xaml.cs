﻿using FirstAED.Models;
using FirstAED.ViewModels;
using System;
using Telerik.XamarinForms.Primitives;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FirstAED.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlarmAlertPage : ContentPage
    {
        AlarmAlertViewModel AAVM;

        public AlarmAlertPage(INavigation Navigation, Alarm alarm)
        {
            InitializeComponent();

            BusyIndicator.AnimationContentColor = Color.FromHex("#aa2129");
            BusyIndicator.AnimationType = AnimationType.Animation4;

            AAVM = new AlarmAlertViewModel(Navigation, alarm);
            
            BindingContext = AAVM;
        }

        private void SlideToActView_SlideCompleted(object sender, EventArgs e)
        {
            NoSlide.IsVisible = false;
            YesSlide.IsVisible = false;
            BusyIndicator.IsBusy = true;
        }
    }
}