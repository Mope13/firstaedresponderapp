﻿using FirstAED.Models;
using FirstAED.Services;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Settings.Abstractions;
using Plugin.Settings;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using FirstAED.ViewModels;
using System.Windows.Input;
using FirstAED.DependencyDefinitions;
using FirstAED.CustomControllers;
using System.Collections.ObjectModel;

namespace FirstAED.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlarmAcceptedPage : ContentPage
    {
        private bool mapElementsAdded = false;
        private Alarm myAlarm;
        private bool moveToStarter = false;

        private ObservableCollection<ResponderDetails> volunteerList;

        Geocoder geoCoder;
        //OrtcService ortcService;
        AlarmAcceptedViewModel alarmAcceptedVM;
        //Alarms alarms;
        bool menuIsExpanded = false;

        public const string ItemSelectedCommandPropertyName = "ItemSelectedCommand";
        public static BindableProperty ItemSelectedCommandProperty = BindableProperty.Create(
            propertyName: "ItemSelectedCommand",
            returnType: typeof(ICommand),
            declaringType: typeof(AlarmAcceptedPage),
            defaultValue: null);

        public ICommand ItemSelectedCommand
        {
            get { return (ICommand)GetValue(ItemSelectedCommandProperty); }
            set { SetValue(ItemSelectedCommandProperty, value); }
        }

        private static ISettings AppSettings => CrossSettings.Current;

        static string UserMapType
        {
            get => AppSettings.GetValueOrDefault(nameof(UserMapType), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(UserMapType), value);
        }

        ResponderDetails _selectedContact;

        public ResponderDetails SelectedContact
        {
            get { return _selectedContact; }
            set
            {
                _selectedContact = value;
                OnPropertyChanged("SelectedContact");
            }
        }

        public AlarmAcceptedPage()
        {

            InitializeComponent();

            
            volunteerList = new ObservableCollection<ResponderDetails>();

            alarmAcceptedVM = new AlarmAcceptedViewModel();
            BindingContext = alarmAcceptedVM;

            emergencyMap.CustomizedPins = new ObservableCollection<CustomizedPin> { };

            AlarmTypeLabel.Text = "Loading...";
            AlarmAddressLabel.Text = "Loading...";
            AlarmInstructionsLabel.Text = "Loading...";
            AlarmNotesLabel.Text = "Loading...";

            //ortcService = ortcServ;
            //ortcService.DataReceived += OrtcService_DataReceived;

            switch (UserMapType)
            {
                case "Satellite":
                    emergencyMap.MapType = MapType.Hybrid;
                    break;
                case "Map":
                    emergencyMap.MapType = MapType.Street;
                    break;
            }


            geoCoder = new Geocoder();
            emergencyMap.IsShowingUser = true;

            Constants.Default.alarmController.AlarmReceivedEvent += AlarmController_AlarmReceivedEvent;
            emergencyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(55.959477, 11.764388), Distance.FromKilometers(200)));
        }

        //private void ResponderMap_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    Debug.WriteLine(e.PropertyName + " just changed!");
        //    if (e.PropertyName == "VisibleRegion" && ResponderMap.VisibleRegion != null)
        //        CalculateBoundingCoordinates(ResponderMap.VisibleRegion);
        //}

        private void MoveToCurrentAlarm(Alarm alarm)
        {
            if (moveToStarter)
            {
                emergencyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(alarm.Starter.Lat, alarm.Starter.Lon), Distance.FromKilometers(3)));
            }
            else
            {
                emergencyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(alarm.Lat, alarm.Lon), Distance.FromKilometers(3)));
            }
        }
        //TODO - messagecenter
        private void AlarmController_AlarmReceivedEvent(Alarm alarm)
        {
            Volunteer myVolunteer = alarm.Volunteers.SingleOrDefault(x => x.Id == Constants.Default.volunteerId);

            if (alarm.Status == (int)Alarm.Statuses.AlarmCompleted || myVolunteer == null || myVolunteer.Response == (int)Volunteer.Responses.VolunteerDecline || myVolunteer.Response == (int)Volunteer.Responses.VolunteerNotNeeded) //Stopped
            {
                Constants.Default.locationController.UpdateLocationInterval(Constants.Default.locationIntervalMinutesDefault);
                Constants.Default.alarmController.AlarmReceivedEvent -= AlarmController_AlarmReceivedEvent;
                Constants.Default.alarmController.Stop();
                Navigation.PopToRootAsync();
            }
            else
            {

                if (!mapElementsAdded)
                {
                    Constants.Default.locationController.UpdateLocationInterval(Constants.Default.locationIntervalSecoundsActiveAlarm);


                    AddAlarmAndStarterPin(alarm);
                    AlarmTypeLabel.Text = alarm.GetAlarmKind(); //LOCALIZE
                    AlarmAddressLabel.Text = alarm.Address;
                    AlarmInstructionsLabel.Text = alarm.GetAlarmInstructions();
                    if(string.IsNullOrEmpty(alarm.Notes))
                    {
                        AlarmNotesLabel.Text = "";
                    }
                    else
                    {
                        AlarmNotesLabel.Text = "Noter: " + alarm.Notes;
                    }

                    MoveToCurrentAlarm(alarm);
                    mapElementsAdded = true;

                }
                
                myAlarm = alarm;

                #region  Add volunteer contacts

                volunteerList.Clear();
                
                foreach(Volunteer v in alarm.Volunteers.Where(x => x.Id != Constants.Default.volunteerId))
                {
                    ResponderDetails rd = new ResponderDetails(v.Name, false, v.Phone);
                   
                    volunteerList.Add(rd);


                    //emergencyMap.UpdateMapPins = true;
                    CustomizedPin oldResponderPin = emergencyMap.CustomizedPins.Where(x => x.Label == v.Name).FirstOrDefault();

                    if (oldResponderPin == null)
                    {

                    }
                    else
                    {
                        emergencyMap.CustomizedPins.Remove(oldResponderPin);
                        emergencyMap.Pins.Remove(emergencyMap.Pins.Single(x => x.Label == v.Name));

                        //oldResponderPin.Pin.Position = new Position(v.Lat, v.Lon);
                        //ResponderMap.Pins.Single(x => x.Label == v.Name).Position = new Position(v.Lat, v.Lon);
                    }

                    CustomizedPin responderPin = new CustomizedPin
                    {
                        Type = PinType.Place,
                        Position = new Position(v.Lat, v.Lon),
                        Label = v.Name,
                        Id = "Responder",
                        CustomPinType = "Responder"
                    };

                    emergencyMap.CustomizedPins.Add(responderPin);
                    emergencyMap.Pins.Add(responderPin);
                }
                if (alarm.Contacts != null)
                {
                    foreach (Contact c in alarm.Contacts)
                    {
                        ResponderDetails rd = new ResponderDetails(c.Name, (c.Secret == 1 ? true : false), c.Phone);

                        volunteerList.Add(rd);
                    }        

                }

                ContactsListView.ItemsSource = volunteerList; //TODO
                #endregion
            }
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            RemoveBinding(ItemSelectedCommandProperty);
            SetBinding(ItemSelectedCommandProperty, new Binding(ItemSelectedCommandPropertyName));
        }

        protected override void OnAppearing()
        {
            //Navigation.RemovePage(Navigation.NavigationStack[0]);
            if (mapElementsAdded)
            {
                MoveToCurrentAlarm(myAlarm);
            }

            base.OnAppearing();
            ContactsListView.SelectedItem = null; //TODO
        }

        protected override void OnDisappearing()
        {

            base.OnDisappearing();
        }

        private void HandleItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return;
            }

            SelectedContact = (ResponderDetails)e.SelectedItem;

            Device.OpenUri(new Uri(string.Format("tel:{0}", SelectedContact.SecretPhoneNumber)));

            var command = ItemSelectedCommand;
            if (command != null && command.CanExecute(e.SelectedItem))
            {
                command.Execute(e.SelectedItem);

            }
        }

        private void OrtcService_DataReceived(Models.MessageReponse mr)
        {
        }

        void OnContactsClicked(object sender, EventArgs e)
        {
            double height0 = this.Height * .27;
            double height1 = this.Height * .75;
            double height2 = this.Height * .37;

            switch (menuIsExpanded)
            {
                case false:
                    //ContactsGrid.LayoutTo(new Rectangle(0, 0, this.Width, height1), 250, Easing.CubicInOut);
                    ContactsView.TranslateTo(0, height2, 250, Easing.CubicInOut);
                    //ContactsLayout.Children.Add(ContactsListView);
                    menuIsExpanded = true;
                    break;
                case true:
                    //ContactsGrid.LayoutTo(new Rectangle(0, 0, this.Width, height0), 250, Easing.CubicInOut);
                    ContactsView.TranslateTo(0, 0, 250, Easing.CubicInOut);
                    //ContactsLayout.Children.Remove(ContactsListView);
                    menuIsExpanded = false;
                    break;
            }
        }

        static void CalculateBoundingCoordinates(MapSpan region)
        {
            var center = region.Center;
            var halfheightDegrees = region.LatitudeDegrees / 2;
            var halfwidthDegrees = region.LongitudeDegrees / 2;

            var left = center.Longitude - halfwidthDegrees;
            var right = center.Longitude + halfwidthDegrees;
            var top = center.Latitude + halfheightDegrees;
            var bottom = center.Latitude - halfheightDegrees;

            if (left < -180) left = 180 + (180 + left);
            if (right > 180) right = (right - 180) - 180;

            Debug.WriteLine("Bounding box:");
            Debug.WriteLine("                    " + top);
            Debug.WriteLine("  " + left + "                " + right);
            Debug.WriteLine("                    " + bottom);
        }

        void OnMapTypeClicked(object sender, EventArgs e)
        {
            emergencyMap.MapType = MapType.Street;
            UserMapType = "Map";
        }

        void OnSatelliteTypeClicked(object sender, EventArgs e)
        {
            emergencyMap.MapType = MapType.Hybrid;
            UserMapType = "Satellite";
        }

        private void AddAlarmAndStarterPin(Alarm alarm)
        {
            CustomizedPin alarmPin = new CustomizedPin
            {
                Type = PinType.Place,
                Position = new Position(alarm.Lat, alarm.Lon),
                Label = alarm.GetAlarmKind(),
                Address = alarm.Address,
                Id = "Alarm",
                CustomPinType = "Alarm"
            };

            emergencyMap.CustomizedPins.Add(alarmPin);
            emergencyMap.Pins.Add(alarmPin);

            Volunteer myVolunteer = alarm.GetVolunteer();
            if (myVolunteer != null && myVolunteer.Response == (int)Volunteer.Responses.VolunteerAED)
            {
                CustomizedPin starterPin = new CustomizedPin
                {
                    Type = PinType.Place,
                    Position = new Position(alarm.Starter.Lat, alarm.Starter.Lon),
                    Label = "Hjertestarter",
                    Address = alarm.Starter.Address,
                    Id = "Starter",
                    CustomPinType = "Starter"
                };

                emergencyMap.CustomizedPins.Add(starterPin);
                emergencyMap.Pins.Add(starterPin);

                //ResponderMap.RouteCoordinates = 

                moveToStarter = true;
            }
        }
    }
}