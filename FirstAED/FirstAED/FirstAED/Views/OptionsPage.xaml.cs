﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FirstAED.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OptionsPage : ContentView
    {
        public static readonly BindableProperty MapTypeLabelTextProperty = BindableProperty.Create("MapTypeLabelText", typeof(string), typeof(OptionsPage));
        public string MapTypeLabelText
        {
            get { return (string)GetValue(MapTypeLabelTextProperty); }
            set { SetValue(MapTypeLabelTextProperty, value); }
        }

        public static readonly BindableProperty MapTypeButtonTextProperty = BindableProperty.Create("MapTypeButtonText", typeof(string), typeof(OptionsPage));
        public string MapTypeButtonText
        {
            get { return (string)GetValue(MapTypeButtonTextProperty); }
            set { SetValue(MapTypeButtonTextProperty, value); }
        }

        public static readonly BindableProperty MapTypeButtonClickedProperty = BindableProperty.Create("MapTypeButtonClicked", typeof(Command), typeof(OptionsPage));
        public Command MapTypeButtonClicked
        {
            get { return (Command)GetValue(MapTypeButtonClickedProperty); }
            set { SetValue(MapTypeButtonClickedProperty, value); }
        }

        public static readonly BindableProperty SatelliteTypeButtonTextProperty = BindableProperty.Create("SatelliteTypeButtonText", typeof(string), typeof(OptionsPage));
        public string SatelliteTypeButtonText
        {
            get { return (string)GetValue(SatelliteTypeButtonTextProperty); }
            set { SetValue(SatelliteTypeButtonTextProperty, value); }
        }

        public static readonly BindableProperty SatelliteTypeButtonClickedProperty = BindableProperty.Create("SatelliteTypeButtonClicked", typeof(Command), typeof(OptionsPage));
        public Command SatelliteTypeButtonClicked
        {
            get { return (Command)GetValue(SatelliteTypeButtonClickedProperty); }
            set { SetValue(SatelliteTypeButtonClickedProperty, value); }
        }

        public static readonly BindableProperty AlarmTypeLabelTextProperty = BindableProperty.Create("AlarmTypeLabelText", typeof(string), typeof(OptionsPage));
        public string AlarmTypeLabelText
        {
            get { return (string)GetValue(AlarmTypeLabelTextProperty); }
            set { SetValue(AlarmTypeLabelTextProperty, value); }
        }

        public static readonly BindableProperty AlarmTypeButtonTextProperty = BindableProperty.Create("AlarmTypeButtonText", typeof(string), typeof(OptionsPage));
        public string AlarmTypeButtonText
        {
            get { return (string)GetValue(AlarmTypeButtonTextProperty); }
            set { SetValue(AlarmTypeButtonTextProperty, value); }
        }

        public static readonly BindableProperty AlarmTypeButtonClickedProperty = BindableProperty.Create("AlarmTypeButtonClicked", typeof(Command), typeof(OptionsPage));
        public Command AlarmTypeButtonClicked
        {
            get { return (Command)GetValue(AlarmTypeButtonClickedProperty); }
            set { SetValue(AlarmTypeButtonClickedProperty, value); }
        }

        public static readonly BindableProperty SirenTypeButtonTextProperty = BindableProperty.Create("SirenTypeButtonText", typeof(string), typeof(OptionsPage));
        public string SirenTypeButtonText
        {
            get { return (string)GetValue(SirenTypeButtonTextProperty); }
            set { SetValue(SirenTypeButtonTextProperty, value); }
        }

        public static readonly BindableProperty SirenTypeButtonClickedProperty = BindableProperty.Create("SirenTypeButtonClicked", typeof(Command), typeof(OptionsPage));
        public Command SirenTypeButtonClicked
        {
            get { return (Command)GetValue(SirenTypeButtonClickedProperty); }
            set { SetValue(SirenTypeButtonClickedProperty, value); }
        }

        public static readonly BindableProperty CancelButtonClickedProperty = BindableProperty.Create("CancelButtonClicked", typeof(Command), typeof(OptionsPage));
        public Command CancelButtonClicked
        {
            get { return (Command)GetValue(CancelButtonClickedProperty); }
            set { SetValue(CancelButtonClickedProperty, value); }
        }


        public OptionsPage()
        {
            InitializeComponent();
        }
    }
}