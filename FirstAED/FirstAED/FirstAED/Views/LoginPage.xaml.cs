﻿using FirstAED.DependencyDefinitions;
using FirstAED.ViewModels;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Telerik.XamarinForms.Primitives;
using Xamarin.Forms;

namespace FirstAED.Views
{
    public partial class LoginPage : ContentPage
    {
        LoginViewModel loginVM;

        string GSA; //GoogleServicesAvailability

        private static ISettings AppSettings => CrossSettings.Current;

        static bool IsUserSet => AppSettings.Contains(nameof(AuthCode));

        static string AuthCode
        {
            get => AppSettings.GetValueOrDefault(nameof(AuthCode), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(AuthCode), value);
        }

        public LoginPage()
        {
            InitializeComponent();
            BusyIndicator.AnimationContentColor = Color.FromHex("#aa2129");
            BusyIndicator.AnimationType = AnimationType.Animation4;
            loginVM = new LoginViewModel(Navigation);
            BindingContext = loginVM;

            


            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    PasswordEntry.BackgroundColor = Color.White;
                    PasswordEntry.TextColor = Color.FromHex("#0f2234");
                    GSA = DependencyService.Get<IGooglePlayServicesAvailability>().IsPlayServicesAvailable();
                    if (LabelGoogleServicesAvailability.Text == "Google Play Services is available!")
                    {
                        LabelGoogleServicesAvailability.TextColor = Color.White;
                        LabelGoogleServicesAvailability.Text = GSA;
                    }
                    break;
                case Device.iOS:
                    PasswordEntry.TextColor = Color.FromHex("#0f2234");
                    break;
            }

            MessagingCenter.Subscribe<LoginViewModel, string>(this, "Fejl", (sender, arg) =>
            {
                BusyIndicator.IsBusy = false;
                LoginButton.IsEnabled = true;
                LoginButton.BorderColor = Color.White;
                DisplayAlert("Fejl", arg, "ok");
            });
        }

        private void Button_Clicked(object sender, System.EventArgs e)
        {
            DisableLoginButton();
        }

        void DisableLoginButton()
        {
            BusyIndicator.IsBusy = true;
            LoginButton.IsEnabled = false;
            LoginButton.BorderColor = Color.FromHex("#66ffffff");
        }

        protected override void OnAppearing()
        {
            if(IsUserSet)
            {
                DisableLoginButton();
                loginVM.LoginClick();
            }
            base.OnAppearing();
        }
    }
}