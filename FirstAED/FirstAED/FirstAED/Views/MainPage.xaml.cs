﻿using FirstAED.Behaviors;
using FirstAED.CustomControllers;
using FirstAED.DependencyDefinitions;
using FirstAED.Models;
using FirstAED.Services;
using FirstAED.ViewModels;
using FirstAED.Views;
using Plugin.FirebasePushNotification;
using Plugin.Geolocator;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace FirstAED
{
    public partial class MainPage : ContentPage
    {
        Geocoder geoCoder;
        //OrtcService ortcService;
        MainViewModel mainVM;
        CaseReportPage caseReportPage;
        Alarms alarms;
        IAlarmService alarmService;
        Button SaveButton;
        Button ActiveNowButton;
        Frame AcceptDateFrame;
        Frame AcceptTimeFrame;
        Label WhenActiveLabel;
        DatePicker AcceptAlarmDatePicker;
        TimePicker AcceptAlarmTimePicker;
        OptionsPage OptionsView;
        public ICommand MapTypeCommand { get; private set; }
        public ICommand SatelliteTypeCommand { get; private set; }
        public ICommand AlarmTypeCommand { get; private set; }
        public ICommand SirenTypeCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        bool menuIsExpanded = false;
        bool loginPageIsRemoved = false;
        private static ISettings AppSettings => CrossSettings.Current;

        static string UserMapType
        {
            get => AppSettings.GetValueOrDefault(nameof(UserMapType), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(UserMapType), value);
        }

        static string SoundIsAlarm
        {
            get => AppSettings.GetValueOrDefault(nameof(SoundIsAlarm), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(SoundIsAlarm), value);
        }

        public MainPage()
        {
            InitializeComponent();
            #region SETTINGS
            AcceptAlarmDatePicker = new DatePicker { BackgroundColor = Color.White, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, TextColor = Color.FromHex("#0f2234"), Format = "ddd dd. MMM", MinimumDate = DateTime.Today };
            AcceptAlarmTimePicker = new TimePicker { BackgroundColor = Color.White, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, TextColor = Color.FromHex("#0f2234"), Format = "HH:mm", Time = DateTime.Now.TimeOfDay };
            AcceptDateFrame = new Frame { Content = AcceptAlarmDatePicker, CornerRadius = 20, VerticalOptions = LayoutOptions.End, Padding = new Thickness(0, 5, 0, 5) };
            AcceptTimeFrame = new Frame { Content = AcceptAlarmTimePicker, CornerRadius = 20, VerticalOptions = LayoutOptions.End, Padding = new Thickness(0, 5, 0, 5) };

            AbsoluteLayout.SetLayoutBounds(AcceptDateFrame, new Rectangle(.5, .3, .6, .2));
            AbsoluteLayout.SetLayoutFlags(AcceptDateFrame, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(AcceptTimeFrame, new Rectangle(.5, .5, .6, .2));
            AbsoluteLayout.SetLayoutFlags(AcceptTimeFrame, AbsoluteLayoutFlags.All);

            WhenActiveLabel = new Label { Text = Constants.Default.whenActive, TextColor = Color.White, HorizontalTextAlignment = TextAlignment.Center };
            AbsoluteLayout.SetLayoutBounds(WhenActiveLabel, new Rectangle(.5, .8, .6, .2));
            AbsoluteLayout.SetLayoutFlags(WhenActiveLabel, AbsoluteLayoutFlags.All);

            ActiveNowButton = new Button { Text = "Aktiv nu", TextColor = Color.White, BackgroundColor = Color.FromHex("#0f2234") };
            ActiveNowButton.Clicked += ActiveNowButton_Clicked;
            SaveButton = new Button { Text = "Gem", TextColor = Color.White, BackgroundColor = Color.FromHex("#0f2234") };
            SaveButton.Clicked += SaveButton_Clicked;
            AbsoluteLayout.SetLayoutBounds(ActiveNowButton, new Rectangle(.1, .9, .3, .15));
            AbsoluteLayout.SetLayoutFlags(ActiveNowButton, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(SaveButton, new Rectangle(.9, .9, .3, .15));
            AbsoluteLayout.SetLayoutFlags(SaveButton, AbsoluteLayoutFlags.All);
            #endregion

            #region OPTIONS
            Image OptionsImage = new Image { Source = "vaerktoj58px.png", Aspect = Aspect.Fill };
            Frame OptionsFrame = new Frame { Content = OptionsImage, BackgroundColor = Color.Gray, CornerRadius = 30, Padding = 0 };
            AbsoluteLayout.SetLayoutBounds(OptionsFrame, new Rectangle(.03, .9, 60, 60));
            AbsoluteLayout.SetLayoutFlags(OptionsFrame, AbsoluteLayoutFlags.PositionProportional);
            MainLayout.Children.Add(OptionsFrame);

            TapGestureRecognizer optionsTapped = new TapGestureRecognizer();
            optionsTapped.Tapped += OptionsTapped_Tapped;
            OptionsFrame.GestureRecognizers.Add(optionsTapped);

            MapTypeCommand = new Command(OnMapTypeClicked);
            SatelliteTypeCommand = new Command(OnSatelliteTypeClicked);
            AlarmTypeCommand = new Command(OnAlarmTypeClicked);
            SirenTypeCommand = new Command(OnSirenTypeClicked);
            CancelCommand = new Command(OnCancelClicked);

            OptionsView = new OptionsPage
            {
                MapTypeLabelText = "Korttype",
                MapTypeButtonText = "Kort", //LOCALIZATION
                MapTypeButtonClicked = (Command)MapTypeCommand,
                SatelliteTypeButtonText = "Satellit",
                SatelliteTypeButtonClicked = (Command)SatelliteTypeCommand,
                AlarmTypeLabelText = "Alarmlyd",
                AlarmTypeButtonText = "Alarm",
                AlarmTypeButtonClicked = (Command)AlarmTypeCommand,
                SirenTypeButtonText = "Ambulance",
                SirenTypeButtonClicked = (Command)SirenTypeCommand,
                CancelButtonClicked = (Command)CancelCommand
            };

            AbsoluteLayout.SetLayoutBounds(OptionsView, new Rectangle(.5, .5, .85, .65));
            AbsoluteLayout.SetLayoutFlags(OptionsView, AbsoluteLayoutFlags.All);

            MainPageLayout.Children.Add(OptionsView);
            OptionsView.IsVisible = false;

            #endregion

            //BadgeIcon.Text = "9+";
            BadgeIcon.IsVisible = false; //TODO
            CaseReportFrame.IsVisible = false;

            VersionLabel.Text = "v" + DependencyService.Get<IBuildNumber>().Version;

            mainVM = new MainViewModel(Navigation);
            BindingContext = mainVM;

            alarmService = new AlarmService();
            alarms = new Alarms();

            NavigationPage.SetHasNavigationBar(this, false);

            //ortcService = ortcServ;
            //ortcService.DataReceived += OrtcService_DataReceived;


            geoCoder = new Geocoder();

            switch (UserMapType)
            {
                case "Satellite":
                    emergencyMap.MapType = MapType.Hybrid;
                    break;
                case "Map":
                    emergencyMap.MapType = MapType.Street;
                    break;
            }



            emergencyMap.IsShowingUser = true;
            emergencyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(55.959477, 11.764388), Xamarin.Forms.Maps.Distance.FromKilometers(200)));

            //ResponderMap.HeartStarterPins = new ObservableCollection<HeartStarterPin> { };

            Device.BeginInvokeOnMainThread(async () =>
            {
                await AddHeartStarterPins();
            });

            //ResponderMap.PropertyChanged += (sender, e) =>
            //{
            //    Debug.WriteLine(e.PropertyName + " just changed!");
            //    if (e.PropertyName == "VisibleRegion" && ResponderMap.VisibleRegion != null)
            //        CalculateBoundingCoordinates(ResponderMap.VisibleRegion);
            //};

            CrossGeolocator.Current.PositionChanged += Current_PositionChanged;


            CrossFirebasePushNotification.Current.OnNotificationReceived += Current_OnNotificationReceived;
            
        }


        private void Current_PositionChanged(object sender, Plugin.Geolocator.Abstractions.PositionEventArgs e)
        {
            var position = e.Position;
            emergencyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude), Xamarin.Forms.Maps.Distance.FromKilometers(2)));
            CrossGeolocator.Current.PositionChanged -= Current_PositionChanged;
        }

        private async void CheckForActiveAlarm()
        {
            Alarm alarm = await alarmService.GetActiveAlarm();
            if (alarm != null)
            {
                Volunteer myVolunteer = alarm.Volunteers.SingleOrDefault(x => x.Id == Constants.Default.volunteerId);
                if (alarm.Status != (int)Alarm.Statuses.AlarmCompleted && myVolunteer != null && myVolunteer.Response != (int)Volunteer.Responses.VolunteerDecline && myVolunteer.Response != (int)Volunteer.Responses.VolunteerNotNeeded)
                {

                    Constants.Default.alarmController.Start();

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (myVolunteer.Response == (int)Volunteer.Responses.VolunteerNotAnswered)
                        {
                            AlarmAlertPage alarmAlertPage = new AlarmAlertPage(Navigation, alarm);
                            NavigationPage.SetHasNavigationBar(alarmAlertPage, false);
                            Navigation.PushAsync(alarmAlertPage);
                        }
                        else
                        {
                            AlarmAcceptedPage alarmAcceptedPage = new AlarmAcceptedPage();
                            NavigationPage.SetHasNavigationBar(alarmAcceptedPage, false);
                            Navigation.PushAsync(alarmAcceptedPage);
                        }
                    });
                }
            }
        }


        private async void Current_OnNotificationReceived(object source, Plugin.FirebasePushNotification.Abstractions.FirebasePushNotificationDataEventArgs e)
        {
            try
            {
                Debug.WriteLine("push received");
                string action = e.Data["action"].ToString();

                switch (action)
                {
                    case "location":
                        await Constants.Default.locationController.PushLocation();
                        break;
                    case "alarm":
                        ShowAlarmAlert();
                        break;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

        }

        private async void ShowAlarmAlert()
        {

            Alarm myAlarm = await alarmService.GetActiveAlarm();
            if (myAlarm != null && myAlarm.Status != 2 && Constants.Default.alarmController.currentAlarmControllerState == AlarmController.AlarmControllerState.Idle) //Not null and not stopped
            {
                Constants.Default.alarmController.currentAlarmControllerState = AlarmController.AlarmControllerState.Active;
                DependencyService.Get<IBackgroundService>().PushToForeground();
                Constants.Default.alarmController.Start();

                AlarmAlertPage alarmAlertPage = new AlarmAlertPage(Navigation, myAlarm);
                NavigationPage.SetHasNavigationBar(alarmAlertPage, false);


                Device.BeginInvokeOnMainThread(() =>
                    {
                        Navigation.PushAsync(alarmAlertPage);
                    });
            }
        }

        protected override void OnAppearing()
        {
            if (!loginPageIsRemoved)
            {
                Navigation.RemovePage(Navigation.NavigationStack[0]);
                CheckForActiveAlarm();

                loginPageIsRemoved = true;
            }
            base.OnAppearing();
        }

        private async Task GetAlarmTask()
        {
            alarms = await alarmService.GetAlarms();
        }

        private void OrtcService_DataReceived(MessageReponse mr)
        {
        }

        void OnCaseReportClick(object sender, EventArgs e)
        {
            caseReportPage = new CaseReportPage();
            NavigationPage.SetHasNavigationBar(caseReportPage, false);
            Navigation.PushAsync(caseReportPage);
        }


        void OnMapTypeClicked()
        {
            emergencyMap.MapType = MapType.Street;
            UserMapType = "Map";
        }

        void OnSatelliteTypeClicked()
        {
            emergencyMap.MapType = MapType.Hybrid;
            UserMapType = "Satellite";
        }

        void OnSirenTypeClicked()
        {
            DependencyService.Get<IAudioService>().PlayAlarm("siren");
            SoundIsAlarm = "siren";
        }

        void OnAlarmTypeClicked()
        {
            DependencyService.Get<IAudioService>().PlayAlarm("alarm");
            SoundIsAlarm = "alarm";
        }

        private async void ActiveNowButton_Clicked(object sender, EventArgs e)
        {
            SetActiveService setActiveService = new SetActiveService();
            DateTime active = new DateTime(2000, 1, 1);
            bool result = await setActiveService.SetActive(active);
            if (result)
            {
                WhenActiveLabel.Text = (DateTime.Now > active ? "Aktiv nu" : "Aktiv fra: " + active.ToString());
            }
        }

        private async void SaveButton_Clicked(object sender, EventArgs e)
        {
            DateTime active = new DateTime(AcceptAlarmDatePicker.Date.Year, AcceptAlarmDatePicker.Date.Month, AcceptAlarmDatePicker.Date.Day, AcceptAlarmTimePicker.Time.Hours, AcceptAlarmTimePicker.Time.Minutes, AcceptAlarmTimePicker.Time.Seconds);

            SetActiveService setActiveService = new SetActiveService();
            bool result = await setActiveService.SetActive(active);

            if (result)
            {
                WhenActiveLabel.Text = (DateTime.Now > active ? "Aktiv nu" : "Aktiv fra: " + active.ToString());
            }
            else
            {

            }
        }
        void OnSettingsClick(object sender, EventArgs e)
        {
            double height0 = this.Height * .27;
            double height1 = this.Height * .75;
            double height2 = this.Height * .48;

            switch (menuIsExpanded)
            {
                case false:
                    SettingsGrid.LayoutTo(new Rectangle(0, 0, this.Width, height1), 250, Easing.CubicInOut);
                    SettingsFrame.TranslateTo(0, height2, 250, Easing.CubicInOut);
                    SettingsLayout.Children.Add(AcceptDateFrame);
                    SettingsLayout.Children.Add(AcceptTimeFrame);
                    SettingsLayout.Children.Add(SaveButton);
                    SettingsLayout.Children.Add(ActiveNowButton);
                    SettingsLayout.Children.Add(WhenActiveLabel);
                    menuIsExpanded = true;
                    break;
                case true:
                    SettingsGrid.LayoutTo(new Rectangle(0, 0, this.Width, height0), 250, Easing.CubicInOut);
                    SettingsFrame.TranslateTo(0, 0, 250, Easing.CubicInOut);
                    SettingsLayout.Children.Remove(AcceptDateFrame);
                    SettingsLayout.Children.Remove(AcceptTimeFrame);
                    SettingsLayout.Children.Remove(SaveButton);
                    SettingsLayout.Children.Remove(ActiveNowButton);
                    SettingsLayout.Children.Remove(WhenActiveLabel);
                    menuIsExpanded = false;
                    break;
            }
        }

        private void OptionsTapped_Tapped(object sender, EventArgs e)
        {
            OptionsView.IsVisible = true;
        }

        void OnCancelClicked()
        {
            OptionsView.IsVisible = false;
        }

        static void CalculateBoundingCoordinates(MapSpan region)
        {
            var center = region.Center;
            var halfheightDegrees = region.LatitudeDegrees / 2;
            var halfwidthDegrees = region.LongitudeDegrees / 2;

            var left = center.Longitude - halfwidthDegrees;
            var right = center.Longitude + halfwidthDegrees;
            var top = center.Latitude + halfheightDegrees;
            var bottom = center.Latitude - halfheightDegrees;

            if (left < -180) left = 180 + (180 + left);
            if (right > 180) right = (right - 180) - 180;

            Debug.WriteLine("Bounding box:");
            Debug.WriteLine("                    " + top);
            Debug.WriteLine("  " + left + "                " + right);
            Debug.WriteLine("                    " + bottom);
        }

        private async Task AddHeartStarterPins()
        {
            RestService restService = new RestService();
            GoogleDirectionsService googleDirectionsService = new GoogleDirectionsService();
            emergencyMap.CustomizedPins = new ObservableCollection<CustomizedPin> { };

            var heartStarters = await restService.GetHeartStarters();

            foreach (Starter HeartStarter in heartStarters)
            {
                if (HeartStarter.Lat == 0)
                {
                    continue;
                }

                CustomizedPin starterPin = new CustomizedPin
                {
                    Type = PinType.Place,
                    Position = new Position(HeartStarter.Lat, HeartStarter.Lon),
                    Label = "Hjertestarter",
                    Address = HeartStarter.Address,
                    Id = "Starter",
                    CustomPinType = "Starter"
                };

                emergencyMap.CustomizedPins.Add(starterPin);
                emergencyMap.Pins.Add(starterPin);
            };

            //StarterDirections starterDirections = new StarterDirections();
            //starterDirections = await googleDirectionsService.GetDirections(Constants.Default.locationController.CurrentPosition.Latitude, Constants.Default.locationController.CurrentPosition.Longitude, heartStarters.ElementAt<Starter>(27).Lat, heartStarters.ElementAt<Starter>(27).Lon);
        }
    }
}
