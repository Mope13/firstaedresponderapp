﻿using System.Reflection;

[assembly: Xamarin.Forms.Dependency(typeof(FirstAED.Droid.Dependencies.ApplicationNameDroid))]
namespace FirstAED.Droid.Dependencies
{
    public class ApplicationNameDroid
    {
        public string ApplicationName
        {
            get { object[] companyAttributes = (object[])typeof(ApplicationNameDroid).GetTypeInfo().Assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute));

                return ((AssemblyTitleAttribute)companyAttributes[0]).Title;
            }
        }
    }
}