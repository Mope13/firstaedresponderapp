﻿using Android.App;
using Android.Gms.Common;
using Android.Util;
using Firebase.Iid;
using FirstAED.DependencyDefinitions;

[assembly: Xamarin.Forms.Dependency(typeof(FirstAED.Dependencies.GooglePlayServicesAvailability))]
namespace FirstAED.Dependencies
{
    public class GooglePlayServicesAvailability : IGooglePlayServicesAvailability
    {
        const string TAG = "MainActivity";

        public string IsPlayServicesAvailable()
        {
            //Android.Net.Uri sound = Android.Net.Uri.Parse("android.resource://" + Application.Context.PackageName + "/" + Resource.Raw.alarm);

            //FirebasePushNotificationManager.SoundUri = sound;

            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(Application.Context);

            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                {
                    return "Please contact system admin and provide them with the following code: " + GoogleApiAvailability.Instance.GetErrorString(resultCode);
                }
                else
                {
                    return "This device is not supported";
                }
            }
            else
            {
                Log.Debug(TAG, "InstanceID token: " + FirebaseInstanceId.Instance.Token);
                return "Google Play Services is available!";
            }
        }
    }
}