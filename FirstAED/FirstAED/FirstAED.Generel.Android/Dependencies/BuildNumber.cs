﻿using FirstAED.Dependencies;
using FirstAED.DependencyDefinitions;
using Xamarin.Forms;

[assembly: Dependency(typeof(BuildNumber))]
namespace FirstAED.Dependencies
{
    class BuildNumber : Java.Lang.Object, IBuildNumber
    {
        public string Version
        {
            get
            {
                var context = Forms.Context;
                return context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionName;
            }
        }
    }
}