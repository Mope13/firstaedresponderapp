﻿using Firebase.Iid;
using FirstAED.DependencyDefinitions;

[assembly: Xamarin.Forms.Dependency(typeof(FirstAED.Dependencies.FirebaseIID))]
namespace FirstAED.Dependencies
{
    public class FirebaseIID : IFirebaseIID
    {
        public string GetFirebaseIID()
        {
            return FirebaseInstanceId.Instance.Token;
        }
    }
}