﻿using Android.Content;
using FirstAED.Dependencies;
using FirstAED.DependencyDefinitions;
using FirstAED.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(BackgroundService))]
namespace FirstAED.Dependencies
{
    public class BackgroundService : Android.App.Application, IBackgroundService
    {
        public void PushToForeground()
        {
            var intent = new Intent(Context, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.SingleTop);
            Context.StartActivity(intent);
        }
    }
}