﻿using System;
using Android.App;
using Android.Content;
using Android.Support.V4.App;
using FirstAED.DependencyDefinitions;
using Android.Media;
using System.Drawing;

[assembly: Xamarin.Forms.Dependency(typeof(FirstAED.Droid.Dependencies.LocalNotification))]
namespace FirstAED.Droid.Dependencies
{
    public class LocalNotification : ILocalNotification
    {
        private static readonly int NotificationId = 1000;

        /// <summary>
        /// Håndterer lokale notfication på android platform
        /// </summary>
        /// <param name="message"></param>
        public void GetLocalNotification(string message)
        {
            // Build the notification:
            NotificationCompat.Builder builder = new NotificationCompat.Builder(Application.Context)
                .SetAutoCancel(true)                    // Dismiss from the notif. area when clicked
                .SetContentTitle("Akutsystem")      // Set its title
                .SetSmallIcon(Resource.Drawable.groenpil58px)  // Display this icon
                .SetDefaults(0 | 2) // Sets sound and vibration
                //.SetSound(sound) // Sets custom sound
                .SetColor(Color.DarkRed.ToArgb()) // Set title text colour
                .SetLights(Color.Red.ToArgb(), 2, 1) // Set LED light
                .SetContentText(message); // The message to display.

            // Finally, publish the notification:
            NotificationManager notificationManager =
                (NotificationManager)Application.Context.GetSystemService(Context.NotificationService);
            notificationManager.Notify(NotificationId, builder.Build());
        }
    }
}