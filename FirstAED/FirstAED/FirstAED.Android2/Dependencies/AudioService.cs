﻿using FirstAED.Dependencies;
using FirstAED.DependencyDefinitions;
using Xamarin.Forms;

[assembly: Dependency(typeof(AudioService))]
namespace FirstAED.Dependencies
{
    class AudioService : IAudioService
    {
        Android.Media.MediaPlayer player;

        public void PlayAlarm(string sound)
        {
            switch (sound)
            {
                case "alarm":

                    using (player = Android.Media.MediaPlayer.Create(global::Android.App.Application.Context, Resource.Raw.alarm))
                    {
                        player.Start();
                    }
                    break;
                case "siren":
                    using (player = Android.Media.MediaPlayer.Create(global::Android.App.Application.Context, Resource.Raw.siren))
                    {
                        player.Start();
                    }
                    break;
            }
        }

        public void StopAlarm()
        {
            player.Stop();
        }
    }
}