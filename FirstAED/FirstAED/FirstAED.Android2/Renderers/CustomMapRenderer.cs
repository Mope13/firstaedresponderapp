﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.Android;
using FirstAED;
using FirstAED.Droid.Renderers;
using System.Linq;
using FirstAED.Models;
using FirstAED.CustomControllers;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace FirstAED.Droid.Renderers
{
    public class CustomMapRenderer : MapRenderer, GoogleMap.IInfoWindowAdapter
    {
        bool isDrawn;
        bool pinsAdded = false;

        bool pinIsSelected = false;
        bool isMapReady;
        Android.Views.View view;

        public CustomMapRenderer(Context context) : base(context)
        {

        }

        public void AddPins()
        {
            NativeMap.Clear();

            if(((CustomMap)Element).HeartStarterPins != null)
            foreach (var pin in ((CustomMap)Element).HeartStarterPins)
            {
                var marker = new MarkerOptions();
                marker.SetPosition(new LatLng(pin.Pin.Position.Latitude, pin.Pin.Position.Longitude));
                marker.SetTitle(pin.Pin.Label);
                marker.SetSnippet(pin.Pin.Address);
                marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.groenpil58px));

                NativeMap.AddMarker(marker);
            }
            if (((CustomMap)Element).AlarmPins != null)
            {
                foreach (var pin in ((CustomMap)Element).AlarmPins)
                {
                    var marker = new MarkerOptions();
                    marker.SetPosition(new LatLng(pin.Pin.Position.Latitude, pin.Pin.Position.Longitude));
                    marker.SetTitle(pin.Pin.Label);
                    marker.SetSnippet(pin.Pin.Address);
                    marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.roedpil58px));

                    NativeMap.AddMarker(marker);
                }
            }
            if (((CustomMap)Element).ResponderPins != null)
            {
                foreach (var pin in ((CustomMap)Element).ResponderPins)
                {
                    var marker = new MarkerOptions();
                    marker.SetPosition(new LatLng(pin.Pin.Position.Latitude, pin.Pin.Position.Longitude));
                    marker.SetTitle(pin.Pin.Label);
                    marker.SetSnippet(pin.Pin.Address);
                    marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.graapil258px));

                    NativeMap.AddMarker(marker);
                }
            }
        }

        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                NativeMap.InfoWindowClick -= OnInfoWindowClick;
            }

        }

        /// <summary>
        /// Updates existing map pins to custom pins
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (!isMapReady && (NativeMap != null))
            {
                NativeMap.SetInfoWindowAdapter(this);
                NativeMap.InfoWindowClick += OnInfoWindowClick;
                isMapReady = true;
            }

            if (e.PropertyName.Equals("VisibleRegion") && !isDrawn && !pinIsSelected)
            {
                if (((CustomMap)Element).HeartStarterPins != null && ((CustomMap)Element).UpdateMapPins == true)
                {
                    NativeMap.Clear();
                    
                    foreach (var pin in ((CustomMap)Element).HeartStarterPins)
                    {
                        var marker = new MarkerOptions();
                        marker.SetPosition(new LatLng(pin.Pin.Position.Latitude, pin.Pin.Position.Longitude));
                        marker.SetTitle(pin.Pin.Label);
                        marker.SetSnippet(pin.Pin.Address);
                        marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.groenpil58px));

                        NativeMap.AddMarker(marker);
                    }
                    if (((CustomMap)Element).AlarmPins != null)
                    {
                        foreach (var pin in ((CustomMap)Element).AlarmPins)
                        {
                            var marker = new MarkerOptions();
                            marker.SetPosition(new LatLng(pin.Pin.Position.Latitude, pin.Pin.Position.Longitude));
                            marker.SetTitle(pin.Pin.Label);
                            marker.SetSnippet(pin.Pin.Address);
                            marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.roedpil58px));

                            NativeMap.AddMarker(marker);
                        }
                    }
                    if (((CustomMap)Element).ResponderPins != null)
                    {
                        foreach (var pin in ((CustomMap)Element).ResponderPins)
                        {
                            var marker = new MarkerOptions();
                            marker.SetPosition(new LatLng(pin.Pin.Position.Latitude, pin.Pin.Position.Longitude));
                            marker.SetTitle(pin.Pin.Label);
                            marker.SetSnippet(pin.Pin.Address);
                            marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.graapil258px));

                            NativeMap.AddMarker(marker);
                        }
                        
                        
                    }
                    isDrawn = true;
                    ((CustomMap)Element).UpdateMapPins = false;
                }
            }
            if (view != null)
            {
                if (!view.IsShown)
                {
                    pinIsSelected = false;
                }
            }
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);

            if (changed)
            {
                isDrawn = false;
            }
        }

        void OnInfoWindowClick(object sender, GoogleMap.InfoWindowClickEventArgs e)
        {
            if (e.Marker.Id == "Heartstarter")
            {
                var customPin = GetStarterPin(e.Marker);

                if (customPin == null)
                {
                    throw new Exception("Custom pin not found");
                }
            }

            if (e.Marker.Id == "Alarm")
            {
                var customPin = GetAlarmPin(e.Marker);

                if (customPin == null)
                {
                    throw new Exception("Custom pin not found");
                }
            }

            if (e.Marker.Id == "Responder")
            {
                var customPin = GetResponderPin(e.Marker);

                if (customPin == null)
                {
                    throw new Exception("Custom pin not found");
                }
            }
        }

        /// <summary>
        /// Sets custom content for info windows
        /// </summary>
        /// <param name="marker"></param>
        /// <returns></returns>
        public Android.Views.View GetInfoContents(Marker marker)
        {
            if (Android.App.Application.Context.GetSystemService(Context.LayoutInflaterService) is Android.Views.LayoutInflater inflater)
            {

                if (marker.Id == "Heartstarter")
                {
                    var customPin = GetStarterPin(marker);
                    if (customPin == null)
                    {
                        throw new Exception("Custom pin not found");
                    }

                    if (customPin.Id == "Heartstarter")
                    {
                        view = inflater.Inflate(Resource.Layout.XamarinMapInfoWindow, null);
                    }
                    else
                    {
                        view = inflater.Inflate(Resource.Layout.MapInfoWindow, null);
                    }

                    var infoTitle = view.FindViewById<TextView>(Resource.Id.InfoWindowTitle);
                    var infoSubtitle = view.FindViewById<TextView>(Resource.Id.InfoWindowSubtitle);

                    if (infoTitle != null)
                    {
                        infoTitle.Text = marker.Title;
                    }
                    if (infoSubtitle != null)
                    {
                        infoSubtitle.Text = marker.Snippet;
                    }

                    return view;
                }

                if (marker.Id == "Alarm")
                {
                    var customPin = GetAlarmPin(marker);
                    if (customPin == null)
                    {
                        throw new Exception("Custom pin not found");
                    }

                    if (customPin.Id == "Alarm")
                    {
                        view = inflater.Inflate(Resource.Layout.XamarinMapInfoWindow, null);
                    }
                    else
                    {
                        view = inflater.Inflate(Resource.Layout.MapInfoWindow, null);
                    }

                    var infoTitle = view.FindViewById<TextView>(Resource.Id.InfoWindowTitle);
                    var infoSubtitle = view.FindViewById<TextView>(Resource.Id.InfoWindowSubtitle);

                    if (infoTitle != null)
                    {
                        infoTitle.Text = marker.Title;
                    }
                    if (infoSubtitle != null)
                    {
                        infoSubtitle.Text = marker.Snippet;
                    }

                    return view;
                }

                if (marker.Id == "Responder")
                {
                    var customPin = GetResponderPin(marker);
                    if (customPin == null)
                    {
                        throw new Exception("Custom pin not found");
                    }

                    if (customPin.Id == "Responder")
                    {
                        view = inflater.Inflate(Resource.Layout.XamarinMapInfoWindow, null);
                    }
                    else
                    {
                        view = inflater.Inflate(Resource.Layout.MapInfoWindow, null);
                    }

                    var infoTitle = view.FindViewById<TextView>(Resource.Id.InfoWindowTitle);
                    var infoSubtitle = view.FindViewById<TextView>(Resource.Id.InfoWindowSubtitle);

                    if (infoTitle != null)
                    {
                        infoTitle.Text = marker.Title;
                    }
                    if (infoSubtitle != null)
                    {
                        infoSubtitle.Text = marker.Snippet;
                    }

                    return view;
                }
                pinIsSelected = true;
            }
            return null;
        }

        public Android.Views.View GetInfoWindow(Marker marker)
        {
            return null;
        }

        HeartStarterPin GetStarterPin(Marker annotation)
        {
            var position = new Position(annotation.Position.Latitude, annotation.Position.Longitude);
            var selection = ((CustomMap)Element).SingleOrDefault(x => x.Position == position);
            var pin = new HeartStarterPin();

            if (selection != null)
            {
                pin.Pin = selection;
                return pin;
            }
            else
            {
                return null;
            }
        }

        AlarmPin GetAlarmPin(Marker annotation)
        {
            var position = new Position(annotation.Position.Latitude, annotation.Position.Longitude);
            var selection = ((CustomMap)Element).SingleOrDefault(x => x.Position == position);
            var pin = new AlarmPin();

            if (selection != null)
            {
                pin.Pin = selection;
                return pin;
            }
            else
            {
                return null;
            }
        }

        ResponderPin GetResponderPin(Marker annotation)
        {
            var position = new Position(annotation.Position.Latitude, annotation.Position.Longitude);
            var selection = ((CustomMap)Element).SingleOrDefault(x => x.Position == position);
            var pin = new ResponderPin();

            if (selection != null)
            {
                pin.Pin = selection;
                return pin;
            }
            else
            {
                return null;
            }
        }
    }
}