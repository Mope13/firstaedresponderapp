﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using Android.App;
//using Android.Content;
//using Android.OS;
//using Android.Runtime;
//using Android.Views;
//using Android.Widget;
//using Firebase.Messaging;
//using Android.Util;
//using FirstAED.Droid;
//using Android.Support.V4.App;
//using FirstAED.Views;

//namespace FirstAED
//{
//    [Service]
//    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
//    public class FirstAEDFirebaseMessagingService : FirebaseMessagingService
//    {
//        Android.Net.Uri sound = Android.Net.Uri.Parse("android.resource://" + Application.Context.PackageName + "/" + Resource.Raw.alarm);
//        const string TAG = "MyFirebaseMsgService";
//        public override void OnMessageReceived(RemoteMessage message)
//        {
//            Log.Debug(TAG, "From: " + message.From);
//            Log.Debug(TAG, "Notification Message Body: " + message.GetNotification().Body);
//            SendNotification(message.GetNotification().Body);
//        }

//        void SendNotification(string messageBody)
//        {
//            Xamarin.Forms.Application.Current.MainPage.Navigation.PushAsync(new AlarmAlertPage(Xamarin.Forms.Application.Current.MainPage.Navigation, messageBody));
//            var intent2 = new Intent(Application.Context, typeof(MainActivity));
//            intent2.AddFlags(ActivityFlags.NewTask);
//            Application.Context.StartActivity(intent2);

//            var intent = new Intent(this, typeof(MainActivity));
//            intent.AddFlags(ActivityFlags.ClearTop);
//            var pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.OneShot);

//            var notificationBuilder = new NotificationCompat.Builder(this)
//                .SetSmallIcon(Resource.Drawable.FirstAED29x29)
//                .SetContentTitle("FirstAED Alarm")
//                .SetContentText(messageBody)
//                .SetAutoCancel(true)
//                .SetContentIntent(pendingIntent)
//                .SetSound(sound);


//            var notificationManager = NotificationManager.FromContext(this);
//            notificationManager.Notify(0, notificationBuilder.Build());
//        }
//    }
//}