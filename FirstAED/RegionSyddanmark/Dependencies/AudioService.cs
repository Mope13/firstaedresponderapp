﻿
using AVFoundation;
using FirstAED.DependencyDefinitions;
using Foundation;
using RegionSyddanmark.Dependencies;
using Xamarin.Forms;

[assembly: Dependency(typeof(AudioService))]
namespace RegionSyddanmark.Dependencies
{
    public class AudioService : IAudioService
    {
        AVAudioPlayer soundEffect;
        NSUrl songURL;

        public void PlayAlarm(string sound)
        {
            NSError err;

            switch (sound)
            {
                case "alarm":
                    // Initialize background music
                    songURL = new NSUrl("alarm.mp3");
                    soundEffect = new AVAudioPlayer(songURL, "mp3", out err);
                    soundEffect.Volume = 100;
                    soundEffect.FinishedPlaying += delegate
                    {
                        soundEffect = null;
                    };
                    soundEffect.NumberOfLoops = 0;
                    soundEffect.Play();
                    break;
                case "siren":
                    // Initialize background music
                    songURL = new NSUrl("siren.mp3");
                    soundEffect = new AVAudioPlayer(songURL, "mp3", out err);
                    soundEffect.Volume = 100;
                    soundEffect.FinishedPlaying += delegate
                    {
                        soundEffect = null;
                    };
                    soundEffect.NumberOfLoops = 0;
                    soundEffect.Play();
                    break;
            }
        }

        public void StopAlarm()
        {
            soundEffect.Stop();
        }
    }
}