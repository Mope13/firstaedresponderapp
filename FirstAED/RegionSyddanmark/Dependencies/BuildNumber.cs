﻿
using FirstAED.DependencyDefinitions;
using Foundation;
using RegionSyddanmark.Dependencies;

[assembly: Xamarin.Forms.Dependency(typeof(BuildNumber))]
namespace RegionSyddanmark.Dependencies
{
    public class BuildNumber : IBuildNumber
    {
        public string Version
        {
            get
            {
                NSObject ver = NSBundle.MainBundle.InfoDictionary["CFBundleShortVersionString"];
                return ver.ToString();
            }
        }
    }
}