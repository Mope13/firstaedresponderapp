﻿using CoreGraphics;
using FirstAED.CustomControllers;
using FirstAED.Models;
using MapKit;
using RegionSyddanmark.Renderers;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace RegionSyddanmark.Renderers
{
    class CustomMapRenderer : MapRenderer
    {
        UIView customPinView;
        ObservableCollection<HeartStarterPin> heartStarterPins;
        ObservableCollection<AlarmPin> alarmPins;
        ObservableCollection<ResponderPin> responderPins;

        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                var nativeMap = Control as MKMapView;
                nativeMap.GetViewForAnnotation = null;
                nativeMap.CalloutAccessoryControlTapped -= OnCalloutAccessoryControlTapped;
                nativeMap.DidSelectAnnotationView -= OnDidSelectAnnotationView;
                nativeMap.DidDeselectAnnotationView -= OnDidDeselectAnnotationView;
            }

            if (e.NewElement != null)
            {
                var formsMap = (CustomMap)e.NewElement;
                var nativeMap = Control as MKMapView;
                heartStarterPins = formsMap.HeartStarterPins;

                nativeMap.GetViewForAnnotation = GetViewForAnnotation;
                nativeMap.CalloutAccessoryControlTapped += OnCalloutAccessoryControlTapped;
                nativeMap.DidSelectAnnotationView += OnDidSelectAnnotationView;
                nativeMap.DidDeselectAnnotationView += OnDidDeselectAnnotationView;
            }
        }

        MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
        {
            MKAnnotationView annotationView = null;

            var anno = annotation as MKPointAnnotation;
            if (anno == null)
            {
                return null;
            }

            if (annotation.GetTitle() == "Hjertestarter")
            {
                var customPin = GetHeartStarterPin(anno);
                if (customPin == null)
                {
                    throw new Exception("Custom pin not found");
                }

                if (customPin.Pin != null)
                {
                    annotationView = mapView.DequeueReusableAnnotation(((CustomMap)Element).Where(x => x.Label == "Heartstarter").ToString());
                    if (annotationView == null)
                    {
                        annotationView = new CustomMKAnnotationView(annotation, customPin.Pin.Label);
                        annotationView.Image = UIImage.FromFile("groenpil58px.png");
                        annotationView.CalloutOffset = new CGPoint(0, 0);
                        annotationView.LeftCalloutAccessoryView = new UIImageView(UIImage.FromFile("groenpil58px.png"));
                        annotationView.RightCalloutAccessoryView = UIButton.FromType(UIButtonType.DetailDisclosure);
                        ((CustomMKAnnotationView)annotationView).Id = customPin.Pin.Label;
                    }
                    annotationView.CanShowCallout = true;
                }
            }

            if (annotation.GetTitle() == "Alarm")
            {
                var customPin = GetHeartStarterPin(anno);
                if (customPin == null)
                {
                    throw new Exception("Custom pin not found");
                }

                if (customPin.Pin != null)
                {
                    annotationView = mapView.DequeueReusableAnnotation(((CustomMap)Element).Where(x => x.Label == "Alarm").ToString());
                    if (annotationView == null)
                    {
                        annotationView = new CustomMKAnnotationView(annotation, customPin.Pin.Label);
                        annotationView.Image = UIImage.FromFile("roedpil58px.png");
                        annotationView.CalloutOffset = new CGPoint(0, 0);
                        annotationView.LeftCalloutAccessoryView = new UIImageView(UIImage.FromFile("roedpil58px.png"));
                        annotationView.RightCalloutAccessoryView = UIButton.FromType(UIButtonType.DetailDisclosure);
                        ((CustomMKAnnotationView)annotationView).Id = customPin.Pin.Label;
                    }
                    annotationView.CanShowCallout = true;
                }
            }

            return annotationView;
        }

        void OnCalloutAccessoryControlTapped(object sender, MKMapViewAccessoryTappedEventArgs e)
        {
            var customView = e.View as CustomMKAnnotationView;
            if (!string.IsNullOrWhiteSpace(customView.Url))
            {
                UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl(customView.Url));
            }
        }

        void OnDidSelectAnnotationView(object sender, MKAnnotationViewEventArgs e)
        {
            var customView = e.View as CustomMKAnnotationView;
            customPinView = new UIView();

            if (customView != null)
            {
                if (customView.Id == "Heartstarter")
                {
                    customPinView.Frame = new CGRect(0, 0, 200, 84);
                    var image = new UIImageView(new CGRect(0, 0, 200, 84));
                    image.Image = UIImage.FromFile("groenpil58px.png");
                    customPinView.AddSubview(image);
                    customPinView.Center = new CGPoint(0, -(e.View.Frame.Height + 75));
                    e.View.AddSubview(customPinView);
                }
                else if (customView.Id == "Alarm")
                {
                    customPinView.Frame = new CGRect(0, 0, 200, 84);
                    var image = new UIImageView(new CGRect(0, 0, 200, 84));
                    image.Image = UIImage.FromFile("roedpil58px.png");
                    customPinView.AddSubview(image);
                    customPinView.Center = new CGPoint(0, -(e.View.Frame.Height + 75));
                    e.View.AddSubview(customPinView);
                }
                else if (customView.Id == "Responder")
                {
                    customPinView.Frame = new CGRect(0, 0, 200, 84);
                    var image = new UIImageView(new CGRect(0, 0, 200, 84));
                    image.Image = UIImage.FromFile("graapil258px.png");
                    customPinView.AddSubview(image);
                    customPinView.Center = new CGPoint(0, -(e.View.Frame.Height + 75));
                    e.View.AddSubview(customPinView);
                }
            }
        }

        void OnDidDeselectAnnotationView(object sender, MKAnnotationViewEventArgs e)
        {
            if (!e.View.Selected)
            {
                customPinView.RemoveFromSuperview();
                customPinView.Dispose();
                customPinView = null;
            }
        }

        HeartStarterPin GetHeartStarterPin(MKPointAnnotation annotation)
        {
            var position = new Position(annotation.Coordinate.Latitude, annotation.Coordinate.Longitude);
            var selection = ((CustomMap)Element).SingleOrDefault(x => x.Position == position);

            var pin = new HeartStarterPin();

            if (selection != null)
            {
                pin.Pin = selection;
                pin.Id = "Heartstarter";
                return pin;
            }
            else
            {
                return null;
            }
        }

        AlarmPin GetAlarmPin(MKPointAnnotation annotation)
        {
            var position = new Position(annotation.Coordinate.Latitude, annotation.Coordinate.Longitude);
            var selection = ((CustomMap)Element).SingleOrDefault(x => x.Position == position);

            var pin = new AlarmPin();

            if (selection != null)
            {
                pin.Pin = selection;
                return pin;
            }
            else
            {
                return null;
            }
        }

        ResponderPin GetResponderPin(MKPointAnnotation annotation)
        {
            var position = new Position(annotation.Coordinate.Latitude, annotation.Coordinate.Longitude);
            var selection = ((CustomMap)Element).SingleOrDefault(x => x.Position == position);

            var pin = new ResponderPin();

            if (selection != null)
            {
                pin.Pin = selection;
                return pin;
            }
            else
            {
                return null;
            }
        }
    }
}