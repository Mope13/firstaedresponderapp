﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Util;
using Android.Views;
using Plugin.FirebasePushNotification;

namespace FirstAED.Droid2
{
    [Activity(Label = "Akutudkald", Icon = "@drawable/ic_launcher", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        const string TAG = "MainActivity";

        public override void OnBackPressed()
        {
            // This prevents a user from being able to hit the back button and leave the login page.
            return;
            base.OnBackPressed();

        }

        public override void OnAttachedToWindow()
        {
            base.OnAttachedToWindow();

            Window.AddFlags(WindowManagerFlags.ShowWhenLocked |
           WindowManagerFlags.KeepScreenOn |
           WindowManagerFlags.DismissKeyguard |
           WindowManagerFlags.TurnScreenOn);

        }

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());


            FirebasePushNotificationManager.ProcessIntent(Intent);

            if (Intent.Extras != null)
            {
                foreach (var key in Intent.Extras.KeySet())
                {
                    var value = Intent.Extras.GetString(key);
                    Log.Debug(TAG, "Key: {0} Value: {1}", key, value);
                }

                string currentPage = Intent.Extras.GetString("currentpage");
                if (currentPage != null)
                {

                }
            }
        }

    }
}

